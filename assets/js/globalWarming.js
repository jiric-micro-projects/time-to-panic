import filter from './util'

const input = [
  {
    label: 2,
    value: 276.112,
  },
  {
    label: 3,
    value: 276.163,
  },
  {
    label: 4,
    value: 276.218,
  },
  {
    label: 5,
    value: 276.276,
  },
  {
    label: 6,
    value: 276.337,
  },
  {
    label: 7,
    value: 276.399,
  },
  {
    label: 8,
    value: 276.464,
  },
  {
    label: 9,
    value: 276.531,
  },
  {
    label: 10,
    value: 276.599,
  },
  {
    label: 11,
    value: 276.668,
  },
  {
    label: 12,
    value: 276.737,
  },
  {
    label: 13,
    value: 276.808,
  },
  {
    label: 14,
    value: 276.878,
  },
  {
    label: 15,
    value: 276.948,
  },
  {
    label: 16,
    value: 277.017,
  },
  {
    label: 17,
    value: 277.086,
  },
  {
    label: 18,
    value: 277.153,
  },
  {
    label: 19,
    value: 277.219,
  },
  {
    label: 20,
    value: 277.283,
  },
  {
    label: 21,
    value: 277.345,
  },
  {
    label: 22,
    value: 277.404,
  },
  {
    label: 23,
    value: 277.461,
  },
  {
    label: 24,
    value: 277.515,
  },
  {
    label: 25,
    value: 277.565,
  },
  {
    label: 26,
    value: 277.612,
  },
  {
    label: 27,
    value: 277.654,
  },
  {
    label: 28,
    value: 277.692,
  },
  {
    label: 29,
    value: 277.726,
  },
  {
    label: 30,
    value: 277.754,
  },
  {
    label: 31,
    value: 277.777,
  },
  {
    label: 32,
    value: 277.796,
  },
  {
    label: 33,
    value: 277.81,
  },
  {
    label: 34,
    value: 277.82,
  },
  {
    label: 35,
    value: 277.825,
  },
  {
    label: 36,
    value: 277.827,
  },
  {
    label: 37,
    value: 277.826,
  },
  {
    label: 38,
    value: 277.821,
  },
  {
    label: 39,
    value: 277.813,
  },
  {
    label: 40,
    value: 277.802,
  },
  {
    label: 41,
    value: 277.788,
  },
  {
    label: 42,
    value: 277.773,
  },
  {
    label: 43,
    value: 277.755,
  },
  {
    label: 44,
    value: 277.735,
  },
  {
    label: 45,
    value: 277.714,
  },
  {
    label: 46,
    value: 277.692,
  },
  {
    label: 47,
    value: 277.668,
  },
  {
    label: 48,
    value: 277.644,
  },
  {
    label: 49,
    value: 277.619,
  },
  {
    label: 50,
    value: 277.593,
  },
  {
    label: 51,
    value: 277.568,
  },
  {
    label: 52,
    value: 277.543,
  },
  {
    label: 53,
    value: 277.518,
  },
  {
    label: 54,
    value: 277.494,
  },
  {
    label: 55,
    value: 277.471,
  },
  {
    label: 56,
    value: 277.449,
  },
  {
    label: 57,
    value: 277.428,
  },
  {
    label: 58,
    value: 277.408,
  },
  {
    label: 59,
    value: 277.39,
  },
  {
    label: 60,
    value: 277.374,
  },
  {
    label: 61,
    value: 277.358,
  },
  {
    label: 62,
    value: 277.344,
  },
  {
    label: 63,
    value: 277.331,
  },
  {
    label: 64,
    value: 277.319,
  },
  {
    label: 65,
    value: 277.309,
  },
  {
    label: 66,
    value: 277.299,
  },
  {
    label: 67,
    value: 277.291,
  },
  {
    label: 68,
    value: 277.283,
  },
  {
    label: 69,
    value: 277.277,
  },
  {
    label: 70,
    value: 277.272,
  },
  {
    label: 71,
    value: 277.268,
  },
  {
    label: 72,
    value: 277.264,
  },
  {
    label: 73,
    value: 277.262,
  },
  {
    label: 74,
    value: 277.26,
  },
  {
    label: 75,
    value: 277.26,
  },
  {
    label: 76,
    value: 277.26,
  },
  {
    label: 77,
    value: 277.261,
  },
  {
    label: 78,
    value: 277.262,
  },
  {
    label: 79,
    value: 277.265,
  },
  {
    label: 80,
    value: 277.268,
  },
  {
    label: 81,
    value: 277.272,
  },
  {
    label: 82,
    value: 277.276,
  },
  {
    label: 83,
    value: 277.282,
  },
  {
    label: 84,
    value: 277.287,
  },
  {
    label: 85,
    value: 277.294,
  },
  {
    label: 86,
    value: 277.3,
  },
  {
    label: 87,
    value: 277.308,
  },
  {
    label: 88,
    value: 277.315,
  },
  {
    label: 89,
    value: 277.324,
  },
  {
    label: 90,
    value: 277.332,
  },
  {
    label: 91,
    value: 277.341,
  },
  {
    label: 92,
    value: 277.351,
  },
  {
    label: 93,
    value: 277.36,
  },
  {
    label: 94,
    value: 277.37,
  },
  {
    label: 95,
    value: 277.381,
  },
  {
    label: 96,
    value: 277.391,
  },
  {
    label: 97,
    value: 277.402,
  },
  {
    label: 98,
    value: 277.413,
  },
  {
    label: 99,
    value: 277.423,
  },
  {
    label: 100,
    value: 277.435,
  },
  {
    label: 101,
    value: 277.446,
  },
  {
    label: 102,
    value: 277.457,
  },
  {
    label: 103,
    value: 277.468,
  },
  {
    label: 104,
    value: 277.479,
  },
  {
    label: 105,
    value: 277.49,
  },
  {
    label: 106,
    value: 277.502,
  },
  {
    label: 107,
    value: 277.513,
  },
  {
    label: 108,
    value: 277.524,
  },
  {
    label: 109,
    value: 277.536,
  },
  {
    label: 110,
    value: 277.547,
  },
  {
    label: 111,
    value: 277.56,
  },
  {
    label: 112,
    value: 277.572,
  },
  {
    label: 113,
    value: 277.585,
  },
  {
    label: 114,
    value: 277.599,
  },
  {
    label: 115,
    value: 277.613,
  },
  {
    label: 116,
    value: 277.628,
  },
  {
    label: 117,
    value: 277.643,
  },
  {
    label: 118,
    value: 277.66,
  },
  {
    label: 119,
    value: 277.677,
  },
  {
    label: 120,
    value: 277.696,
  },
  {
    label: 121,
    value: 277.715,
  },
  {
    label: 122,
    value: 277.736,
  },
  {
    label: 123,
    value: 277.757,
  },
  {
    label: 124,
    value: 277.781,
  },
  {
    label: 125,
    value: 277.805,
  },
  {
    label: 126,
    value: 277.831,
  },
  {
    label: 127,
    value: 277.858,
  },
  {
    label: 128,
    value: 277.887,
  },
  {
    label: 129,
    value: 277.917,
  },
  {
    label: 130,
    value: 277.95,
  },
  {
    label: 131,
    value: 277.984,
  },
  {
    label: 132,
    value: 278.019,
  },
  {
    label: 133,
    value: 278.057,
  },
  {
    label: 134,
    value: 278.097,
  },
  {
    label: 135,
    value: 278.139,
  },
  {
    label: 136,
    value: 278.183,
  },
  {
    label: 137,
    value: 278.229,
  },
  {
    label: 138,
    value: 278.277,
  },
  {
    label: 139,
    value: 278.327,
  },
  {
    label: 140,
    value: 278.379,
  },
  {
    label: 141,
    value: 278.433,
  },
  {
    label: 142,
    value: 278.488,
  },
  {
    label: 143,
    value: 278.544,
  },
  {
    label: 144,
    value: 278.602,
  },
  {
    label: 145,
    value: 278.661,
  },
  {
    label: 146,
    value: 278.72,
  },
  {
    label: 147,
    value: 278.781,
  },
  {
    label: 148,
    value: 278.842,
  },
  {
    label: 149,
    value: 278.903,
  },
  {
    label: 150,
    value: 278.965,
  },
  {
    label: 151,
    value: 279.027,
  },
  {
    label: 152,
    value: 279.089,
  },
  {
    label: 153,
    value: 279.152,
  },
  {
    label: 154,
    value: 279.214,
  },
  {
    label: 155,
    value: 279.275,
  },
  {
    label: 156,
    value: 279.336,
  },
  {
    label: 157,
    value: 279.397,
  },
  {
    label: 158,
    value: 279.456,
  },
  {
    label: 159,
    value: 279.515,
  },
  {
    label: 160,
    value: 279.573,
  },
  {
    label: 161,
    value: 279.629,
  },
  {
    label: 162,
    value: 279.685,
  },
  {
    label: 163,
    value: 279.738,
  },
  {
    label: 164,
    value: 279.79,
  },
  {
    label: 165,
    value: 279.841,
  },
  {
    label: 166,
    value: 279.889,
  },
  {
    label: 167,
    value: 279.935,
  },
  {
    label: 168,
    value: 279.979,
  },
  {
    label: 169,
    value: 280.021,
  },
  {
    label: 170,
    value: 280.061,
  },
  {
    label: 171,
    value: 280.098,
  },
  {
    label: 172,
    value: 280.133,
  },
  {
    label: 173,
    value: 280.167,
  },
  {
    label: 174,
    value: 280.198,
  },
  {
    label: 175,
    value: 280.228,
  },
  {
    label: 176,
    value: 280.256,
  },
  {
    label: 177,
    value: 280.283,
  },
  {
    label: 178,
    value: 280.308,
  },
  {
    label: 179,
    value: 280.332,
  },
  {
    label: 180,
    value: 280.354,
  },
  {
    label: 181,
    value: 280.376,
  },
  {
    label: 182,
    value: 280.397,
  },
  {
    label: 183,
    value: 280.417,
  },
  {
    label: 184,
    value: 280.436,
  },
  {
    label: 185,
    value: 280.454,
  },
  {
    label: 186,
    value: 280.472,
  },
  {
    label: 187,
    value: 280.49,
  },
  {
    label: 188,
    value: 280.507,
  },
  {
    label: 189,
    value: 280.524,
  },
  {
    label: 190,
    value: 280.541,
  },
  {
    label: 191,
    value: 280.558,
  },
  {
    label: 192,
    value: 280.575,
  },
  {
    label: 193,
    value: 280.592,
  },
  {
    label: 194,
    value: 280.61,
  },
  {
    label: 195,
    value: 280.628,
  },
  {
    label: 196,
    value: 280.646,
  },
  {
    label: 197,
    value: 280.666,
  },
  {
    label: 198,
    value: 280.686,
  },
  {
    label: 199,
    value: 280.707,
  },
  {
    label: 200,
    value: 280.729,
  },
  {
    label: 201,
    value: 280.752,
  },
  {
    label: 202,
    value: 280.777,
  },
  {
    label: 203,
    value: 280.802,
  },
  {
    label: 204,
    value: 280.829,
  },
  {
    label: 205,
    value: 280.857,
  },
  {
    label: 206,
    value: 280.887,
  },
  {
    label: 207,
    value: 280.916,
  },
  {
    label: 208,
    value: 280.947,
  },
  {
    label: 209,
    value: 280.978,
  },
  {
    label: 210,
    value: 281.009,
  },
  {
    label: 211,
    value: 281.041,
  },
  {
    label: 212,
    value: 281.072,
  },
  {
    label: 213,
    value: 281.103,
  },
  {
    label: 214,
    value: 281.134,
  },
  {
    label: 215,
    value: 281.163,
  },
  {
    label: 216,
    value: 281.193,
  },
  {
    label: 217,
    value: 281.221,
  },
  {
    label: 218,
    value: 281.248,
  },
  {
    label: 219,
    value: 281.274,
  },
  {
    label: 220,
    value: 281.299,
  },
  {
    label: 221,
    value: 281.322,
  },
  {
    label: 222,
    value: 281.343,
  },
  {
    label: 223,
    value: 281.363,
  },
  {
    label: 224,
    value: 281.38,
  },
  {
    label: 225,
    value: 281.394,
  },
  {
    label: 226,
    value: 281.407,
  },
  {
    label: 227,
    value: 281.417,
  },
  {
    label: 228,
    value: 281.423,
  },
  {
    label: 229,
    value: 281.428,
  },
  {
    label: 230,
    value: 281.429,
  },
  {
    label: 231,
    value: 281.427,
  },
  {
    label: 232,
    value: 281.423,
  },
  {
    label: 233,
    value: 281.416,
  },
  {
    label: 234,
    value: 281.407,
  },
  {
    label: 235,
    value: 281.395,
  },
  {
    label: 236,
    value: 281.381,
  },
  {
    label: 237,
    value: 281.365,
  },
  {
    label: 238,
    value: 281.347,
  },
  {
    label: 239,
    value: 281.327,
  },
  {
    label: 240,
    value: 281.306,
  },
  {
    label: 241,
    value: 281.282,
  },
  {
    label: 242,
    value: 281.257,
  },
  {
    label: 243,
    value: 281.23,
  },
  {
    label: 244,
    value: 281.202,
  },
  {
    label: 245,
    value: 281.173,
  },
  {
    label: 246,
    value: 281.142,
  },
  {
    label: 247,
    value: 281.11,
  },
  {
    label: 248,
    value: 281.077,
  },
  {
    label: 249,
    value: 281.043,
  },
  {
    label: 250,
    value: 281.008,
  },
  {
    label: 251,
    value: 280.973,
  },
  {
    label: 252,
    value: 280.937,
  },
  {
    label: 253,
    value: 280.9,
  },
  {
    label: 254,
    value: 280.863,
  },
  {
    label: 255,
    value: 280.826,
  },
  {
    label: 256,
    value: 280.788,
  },
  {
    label: 257,
    value: 280.75,
  },
  {
    label: 258,
    value: 280.712,
  },
  {
    label: 259,
    value: 280.675,
  },
  {
    label: 260,
    value: 280.637,
  },
  {
    label: 261,
    value: 280.6,
  },
  {
    label: 262,
    value: 280.563,
  },
  {
    label: 263,
    value: 280.526,
  },
  {
    label: 264,
    value: 280.491,
  },
  {
    label: 265,
    value: 280.455,
  },
  {
    label: 266,
    value: 280.421,
  },
  {
    label: 267,
    value: 280.387,
  },
  {
    label: 268,
    value: 280.355,
  },
  {
    label: 269,
    value: 280.323,
  },
  {
    label: 270,
    value: 280.293,
  },
  {
    label: 271,
    value: 280.264,
  },
  {
    label: 272,
    value: 280.236,
  },
  {
    label: 273,
    value: 280.21,
  },
  {
    label: 274,
    value: 280.185,
  },
  {
    label: 275,
    value: 280.163,
  },
  {
    label: 276,
    value: 280.141,
  },
  {
    label: 277,
    value: 280.121,
  },
  {
    label: 278,
    value: 280.103,
  },
  {
    label: 279,
    value: 280.086,
  },
  {
    label: 280,
    value: 280.07,
  },
  {
    label: 281,
    value: 280.055,
  },
  {
    label: 282,
    value: 280.041,
  },
  {
    label: 283,
    value: 280.028,
  },
  {
    label: 284,
    value: 280.016,
  },
  {
    label: 285,
    value: 280.004,
  },
  {
    label: 286,
    value: 279.993,
  },
  {
    label: 287,
    value: 279.983,
  },
  {
    label: 288,
    value: 279.973,
  },
  {
    label: 289,
    value: 279.962,
  },
  {
    label: 290,
    value: 279.953,
  },
  {
    label: 291,
    value: 279.943,
  },
  {
    label: 292,
    value: 279.933,
  },
  {
    label: 293,
    value: 279.923,
  },
  {
    label: 294,
    value: 279.913,
  },
  {
    label: 295,
    value: 279.902,
  },
  {
    label: 296,
    value: 279.891,
  },
  {
    label: 297,
    value: 279.879,
  },
  {
    label: 298,
    value: 279.867,
  },
  {
    label: 299,
    value: 279.854,
  },
  {
    label: 300,
    value: 279.839,
  },
  {
    label: 301,
    value: 279.824,
  },
  {
    label: 302,
    value: 279.808,
  },
  {
    label: 303,
    value: 279.791,
  },
  {
    label: 304,
    value: 279.772,
  },
  {
    label: 305,
    value: 279.752,
  },
  {
    label: 306,
    value: 279.731,
  },
  {
    label: 307,
    value: 279.708,
  },
  {
    label: 308,
    value: 279.684,
  },
  {
    label: 309,
    value: 279.659,
  },
  {
    label: 310,
    value: 279.633,
  },
  {
    label: 311,
    value: 279.606,
  },
  {
    label: 312,
    value: 279.577,
  },
  {
    label: 313,
    value: 279.547,
  },
  {
    label: 314,
    value: 279.516,
  },
  {
    label: 315,
    value: 279.484,
  },
  {
    label: 316,
    value: 279.451,
  },
  {
    label: 317,
    value: 279.416,
  },
  {
    label: 318,
    value: 279.38,
  },
  {
    label: 319,
    value: 279.343,
  },
  {
    label: 320,
    value: 279.305,
  },
  {
    label: 321,
    value: 279.266,
  },
  {
    label: 322,
    value: 279.226,
  },
  {
    label: 323,
    value: 279.184,
  },
  {
    label: 324,
    value: 279.141,
  },
  {
    label: 325,
    value: 279.097,
  },
  {
    label: 326,
    value: 279.052,
  },
  {
    label: 327,
    value: 279.006,
  },
  {
    label: 328,
    value: 278.959,
  },
  {
    label: 329,
    value: 278.91,
  },
  {
    label: 330,
    value: 278.86,
  },
  {
    label: 331,
    value: 278.81,
  },
  {
    label: 332,
    value: 278.758,
  },
  {
    label: 333,
    value: 278.706,
  },
  {
    label: 334,
    value: 278.652,
  },
  {
    label: 335,
    value: 278.598,
  },
  {
    label: 336,
    value: 278.543,
  },
  {
    label: 337,
    value: 278.488,
  },
  {
    label: 338,
    value: 278.433,
  },
  {
    label: 339,
    value: 278.377,
  },
  {
    label: 340,
    value: 278.32,
  },
  {
    label: 341,
    value: 278.264,
  },
  {
    label: 342,
    value: 278.207,
  },
  {
    label: 343,
    value: 278.15,
  },
  {
    label: 344,
    value: 278.094,
  },
  {
    label: 345,
    value: 278.037,
  },
  {
    label: 346,
    value: 277.981,
  },
  {
    label: 347,
    value: 277.925,
  },
  {
    label: 348,
    value: 277.869,
  },
  {
    label: 349,
    value: 277.814,
  },
  {
    label: 350,
    value: 277.76,
  },
  {
    label: 351,
    value: 277.706,
  },
  {
    label: 352,
    value: 277.653,
  },
  {
    label: 353,
    value: 277.6,
  },
  {
    label: 354,
    value: 277.549,
  },
  {
    label: 355,
    value: 277.498,
  },
  {
    label: 356,
    value: 277.449,
  },
  {
    label: 357,
    value: 277.401,
  },
  {
    label: 358,
    value: 277.354,
  },
  {
    label: 359,
    value: 277.308,
  },
  {
    label: 360,
    value: 277.264,
  },
  {
    label: 361,
    value: 277.221,
  },
  {
    label: 362,
    value: 277.18,
  },
  {
    label: 363,
    value: 277.14,
  },
  {
    label: 364,
    value: 277.103,
  },
  {
    label: 365,
    value: 277.067,
  },
  {
    label: 366,
    value: 277.033,
  },
  {
    label: 367,
    value: 277.001,
  },
  {
    label: 368,
    value: 276.97,
  },
  {
    label: 369,
    value: 276.942,
  },
  {
    label: 370,
    value: 276.915,
  },
  {
    label: 371,
    value: 276.89,
  },
  {
    label: 372,
    value: 276.866,
  },
  {
    label: 373,
    value: 276.844,
  },
  {
    label: 374,
    value: 276.824,
  },
  {
    label: 375,
    value: 276.805,
  },
  {
    label: 376,
    value: 276.787,
  },
  {
    label: 377,
    value: 276.771,
  },
  {
    label: 378,
    value: 276.757,
  },
  {
    label: 379,
    value: 276.743,
  },
  {
    label: 380,
    value: 276.731,
  },
  {
    label: 381,
    value: 276.721,
  },
  {
    label: 382,
    value: 276.711,
  },
  {
    label: 383,
    value: 276.703,
  },
  {
    label: 384,
    value: 276.695,
  },
  {
    label: 385,
    value: 276.689,
  },
  {
    label: 386,
    value: 276.684,
  },
  {
    label: 387,
    value: 276.68,
  },
  {
    label: 388,
    value: 276.677,
  },
  {
    label: 389,
    value: 276.675,
  },
  {
    label: 390,
    value: 276.674,
  },
  {
    label: 391,
    value: 276.674,
  },
  {
    label: 392,
    value: 276.674,
  },
  {
    label: 393,
    value: 276.676,
  },
  {
    label: 394,
    value: 276.678,
  },
  {
    label: 395,
    value: 276.68,
  },
  {
    label: 396,
    value: 276.684,
  },
  {
    label: 397,
    value: 276.688,
  },
  {
    label: 398,
    value: 276.692,
  },
  {
    label: 399,
    value: 276.697,
  },
  {
    label: 400,
    value: 276.703,
  },
  {
    label: 401,
    value: 276.709,
  },
  {
    label: 402,
    value: 276.715,
  },
  {
    label: 403,
    value: 276.722,
  },
  {
    label: 404,
    value: 276.729,
  },
  {
    label: 405,
    value: 276.737,
  },
  {
    label: 406,
    value: 276.744,
  },
  {
    label: 407,
    value: 276.752,
  },
  {
    label: 408,
    value: 276.76,
  },
  {
    label: 409,
    value: 276.768,
  },
  {
    label: 410,
    value: 276.777,
  },
  {
    label: 411,
    value: 276.785,
  },
  {
    label: 412,
    value: 276.793,
  },
  {
    label: 413,
    value: 276.801,
  },
  {
    label: 414,
    value: 276.81,
  },
  {
    label: 415,
    value: 276.818,
  },
  {
    label: 416,
    value: 276.826,
  },
  {
    label: 417,
    value: 276.833,
  },
  {
    label: 418,
    value: 276.841,
  },
  {
    label: 419,
    value: 276.848,
  },
  {
    label: 420,
    value: 276.855,
  },
  {
    label: 421,
    value: 276.861,
  },
  {
    label: 422,
    value: 276.867,
  },
  {
    label: 423,
    value: 276.873,
  },
  {
    label: 424,
    value: 276.878,
  },
  {
    label: 425,
    value: 276.883,
  },
  {
    label: 426,
    value: 276.887,
  },
  {
    label: 427,
    value: 276.89,
  },
  {
    label: 428,
    value: 276.893,
  },
  {
    label: 429,
    value: 276.895,
  },
  {
    label: 430,
    value: 276.896,
  },
  {
    label: 431,
    value: 276.897,
  },
  {
    label: 432,
    value: 276.897,
  },
  {
    label: 433,
    value: 276.897,
  },
  {
    label: 434,
    value: 276.896,
  },
  {
    label: 435,
    value: 276.894,
  },
  {
    label: 436,
    value: 276.892,
  },
  {
    label: 437,
    value: 276.889,
  },
  {
    label: 438,
    value: 276.886,
  },
  {
    label: 439,
    value: 276.882,
  },
  {
    label: 440,
    value: 276.878,
  },
  {
    label: 441,
    value: 276.873,
  },
  {
    label: 442,
    value: 276.868,
  },
  {
    label: 443,
    value: 276.863,
  },
  {
    label: 444,
    value: 276.857,
  },
  {
    label: 445,
    value: 276.851,
  },
  {
    label: 446,
    value: 276.845,
  },
  {
    label: 447,
    value: 276.839,
  },
  {
    label: 448,
    value: 276.832,
  },
  {
    label: 449,
    value: 276.825,
  },
  {
    label: 450,
    value: 276.817,
  },
  {
    label: 451,
    value: 276.81,
  },
  {
    label: 452,
    value: 276.803,
  },
  {
    label: 453,
    value: 276.795,
  },
  {
    label: 454,
    value: 276.787,
  },
  {
    label: 455,
    value: 276.779,
  },
  {
    label: 456,
    value: 276.772,
  },
  {
    label: 457,
    value: 276.764,
  },
  {
    label: 458,
    value: 276.756,
  },
  {
    label: 459,
    value: 276.748,
  },
  {
    label: 460,
    value: 276.74,
  },
  {
    label: 461,
    value: 276.733,
  },
  {
    label: 462,
    value: 276.725,
  },
  {
    label: 463,
    value: 276.717,
  },
  {
    label: 464,
    value: 276.71,
  },
  {
    label: 465,
    value: 276.703,
  },
  {
    label: 466,
    value: 276.695,
  },
  {
    label: 467,
    value: 276.688,
  },
  {
    label: 468,
    value: 276.681,
  },
  {
    label: 469,
    value: 276.673,
  },
  {
    label: 470,
    value: 276.666,
  },
  {
    label: 471,
    value: 276.659,
  },
  {
    label: 472,
    value: 276.651,
  },
  {
    label: 473,
    value: 276.643,
  },
  {
    label: 474,
    value: 276.635,
  },
  {
    label: 475,
    value: 276.628,
  },
  {
    label: 476,
    value: 276.62,
  },
  {
    label: 477,
    value: 276.611,
  },
  {
    label: 478,
    value: 276.603,
  },
  {
    label: 479,
    value: 276.594,
  },
  {
    label: 480,
    value: 276.585,
  },
  {
    label: 481,
    value: 276.576,
  },
  {
    label: 482,
    value: 276.567,
  },
  {
    label: 483,
    value: 276.557,
  },
  {
    label: 484,
    value: 276.547,
  },
  {
    label: 485,
    value: 276.536,
  },
  {
    label: 486,
    value: 276.526,
  },
  {
    label: 487,
    value: 276.515,
  },
  {
    label: 488,
    value: 276.503,
  },
  {
    label: 489,
    value: 276.491,
  },
  {
    label: 490,
    value: 276.479,
  },
  {
    label: 491,
    value: 276.466,
  },
  {
    label: 492,
    value: 276.452,
  },
  {
    label: 493,
    value: 276.439,
  },
  {
    label: 494,
    value: 276.424,
  },
  {
    label: 495,
    value: 276.409,
  },
  {
    label: 496,
    value: 276.394,
  },
  {
    label: 497,
    value: 276.378,
  },
  {
    label: 498,
    value: 276.361,
  },
  {
    label: 499,
    value: 276.344,
  },
  {
    label: 500,
    value: 276.326,
  },
  {
    label: 501,
    value: 276.308,
  },
  {
    label: 502,
    value: 276.289,
  },
  {
    label: 503,
    value: 276.269,
  },
  {
    label: 504,
    value: 276.25,
  },
  {
    label: 505,
    value: 276.23,
  },
  {
    label: 506,
    value: 276.21,
  },
  {
    label: 507,
    value: 276.19,
  },
  {
    label: 508,
    value: 276.17,
  },
  {
    label: 509,
    value: 276.15,
  },
  {
    label: 510,
    value: 276.13,
  },
  {
    label: 511,
    value: 276.111,
  },
  {
    label: 512,
    value: 276.092,
  },
  {
    label: 513,
    value: 276.074,
  },
  {
    label: 514,
    value: 276.057,
  },
  {
    label: 515,
    value: 276.04,
  },
  {
    label: 516,
    value: 276.025,
  },
  {
    label: 517,
    value: 276.01,
  },
  {
    label: 518,
    value: 275.997,
  },
  {
    label: 519,
    value: 275.984,
  },
  {
    label: 520,
    value: 275.973,
  },
  {
    label: 521,
    value: 275.964,
  },
  {
    label: 522,
    value: 275.956,
  },
  {
    label: 523,
    value: 275.949,
  },
  {
    label: 524,
    value: 275.945,
  },
  {
    label: 525,
    value: 275.942,
  },
  {
    label: 526,
    value: 275.941,
  },
  {
    label: 527,
    value: 275.942,
  },
  {
    label: 528,
    value: 275.945,
  },
  {
    label: 529,
    value: 275.95,
  },
  {
    label: 530,
    value: 275.958,
  },
  {
    label: 531,
    value: 275.968,
  },
  {
    label: 532,
    value: 275.981,
  },
  {
    label: 533,
    value: 275.997,
  },
  {
    label: 534,
    value: 276.015,
  },
  {
    label: 535,
    value: 276.036,
  },
  {
    label: 536,
    value: 276.06,
  },
  {
    label: 537,
    value: 276.087,
  },
  {
    label: 538,
    value: 276.117,
  },
  {
    label: 539,
    value: 276.15,
  },
  {
    label: 540,
    value: 276.185,
  },
  {
    label: 541,
    value: 276.223,
  },
  {
    label: 542,
    value: 276.264,
  },
  {
    label: 543,
    value: 276.306,
  },
  {
    label: 544,
    value: 276.349,
  },
  {
    label: 545,
    value: 276.395,
  },
  {
    label: 546,
    value: 276.442,
  },
  {
    label: 547,
    value: 276.49,
  },
  {
    label: 548,
    value: 276.539,
  },
  {
    label: 549,
    value: 276.588,
  },
  {
    label: 550,
    value: 276.638,
  },
  {
    label: 551,
    value: 276.688,
  },
  {
    label: 552,
    value: 276.739,
  },
  {
    label: 553,
    value: 276.789,
  },
  {
    label: 554,
    value: 276.839,
  },
  {
    label: 555,
    value: 276.888,
  },
  {
    label: 556,
    value: 276.936,
  },
  {
    label: 557,
    value: 276.983,
  },
  {
    label: 558,
    value: 277.029,
  },
  {
    label: 559,
    value: 277.074,
  },
  {
    label: 560,
    value: 277.117,
  },
  {
    label: 561,
    value: 277.158,
  },
  {
    label: 562,
    value: 277.197,
  },
  {
    label: 563,
    value: 277.233,
  },
  {
    label: 564,
    value: 277.267,
  },
  {
    label: 565,
    value: 277.298,
  },
  {
    label: 566,
    value: 277.327,
  },
  {
    label: 567,
    value: 277.352,
  },
  {
    label: 568,
    value: 277.374,
  },
  {
    label: 569,
    value: 277.392,
  },
  {
    label: 570,
    value: 277.406,
  },
  {
    label: 571,
    value: 277.416,
  },
  {
    label: 572,
    value: 277.422,
  },
  {
    label: 573,
    value: 277.423,
  },
  {
    label: 574,
    value: 277.421,
  },
  {
    label: 575,
    value: 277.414,
  },
  {
    label: 576,
    value: 277.405,
  },
  {
    label: 577,
    value: 277.392,
  },
  {
    label: 578,
    value: 277.376,
  },
  {
    label: 579,
    value: 277.358,
  },
  {
    label: 580,
    value: 277.337,
  },
  {
    label: 581,
    value: 277.316,
  },
  {
    label: 582,
    value: 277.292,
  },
  {
    label: 583,
    value: 277.268,
  },
  {
    label: 584,
    value: 277.243,
  },
  {
    label: 585,
    value: 277.217,
  },
  {
    label: 586,
    value: 277.192,
  },
  {
    label: 587,
    value: 277.167,
  },
  {
    label: 588,
    value: 277.142,
  },
  {
    label: 589,
    value: 277.119,
  },
  {
    label: 590,
    value: 277.097,
  },
  {
    label: 591,
    value: 277.077,
  },
  {
    label: 592,
    value: 277.058,
  },
  {
    label: 593,
    value: 277.043,
  },
  {
    label: 594,
    value: 277.029,
  },
  {
    label: 595,
    value: 277.019,
  },
  {
    label: 596,
    value: 277.013,
  },
  {
    label: 597,
    value: 277.01,
  },
  {
    label: 598,
    value: 277.01,
  },
  {
    label: 599,
    value: 277.014,
  },
  {
    label: 600,
    value: 277.021,
  },
  {
    label: 601,
    value: 277.031,
  },
  {
    label: 602,
    value: 277.045,
  },
  {
    label: 603,
    value: 277.06,
  },
  {
    label: 604,
    value: 277.079,
  },
  {
    label: 605,
    value: 277.101,
  },
  {
    label: 606,
    value: 277.124,
  },
  {
    label: 607,
    value: 277.151,
  },
  {
    label: 608,
    value: 277.179,
  },
  {
    label: 609,
    value: 277.209,
  },
  {
    label: 610,
    value: 277.242,
  },
  {
    label: 611,
    value: 277.276,
  },
  {
    label: 612,
    value: 277.313,
  },
  {
    label: 613,
    value: 277.351,
  },
  {
    label: 614,
    value: 277.39,
  },
  {
    label: 615,
    value: 277.431,
  },
  {
    label: 616,
    value: 277.473,
  },
  {
    label: 617,
    value: 277.516,
  },
  {
    label: 618,
    value: 277.56,
  },
  {
    label: 619,
    value: 277.605,
  },
  {
    label: 620,
    value: 277.651,
  },
  {
    label: 621,
    value: 277.697,
  },
  {
    label: 622,
    value: 277.744,
  },
  {
    label: 623,
    value: 277.792,
  },
  {
    label: 624,
    value: 277.839,
  },
  {
    label: 625,
    value: 277.887,
  },
  {
    label: 626,
    value: 277.935,
  },
  {
    label: 627,
    value: 277.983,
  },
  {
    label: 628,
    value: 278.031,
  },
  {
    label: 629,
    value: 278.078,
  },
  {
    label: 630,
    value: 278.125,
  },
  {
    label: 631,
    value: 278.171,
  },
  {
    label: 632,
    value: 278.217,
  },
  {
    label: 633,
    value: 278.262,
  },
  {
    label: 634,
    value: 278.305,
  },
  {
    label: 635,
    value: 278.349,
  },
  {
    label: 636,
    value: 278.391,
  },
  {
    label: 637,
    value: 278.433,
  },
  {
    label: 638,
    value: 278.474,
  },
  {
    label: 639,
    value: 278.514,
  },
  {
    label: 640,
    value: 278.553,
  },
  {
    label: 641,
    value: 278.592,
  },
  {
    label: 642,
    value: 278.63,
  },
  {
    label: 643,
    value: 278.667,
  },
  {
    label: 644,
    value: 278.704,
  },
  {
    label: 645,
    value: 278.74,
  },
  {
    label: 646,
    value: 278.775,
  },
  {
    label: 647,
    value: 278.81,
  },
  {
    label: 648,
    value: 278.844,
  },
  {
    label: 649,
    value: 278.877,
  },
  {
    label: 650,
    value: 278.91,
  },
  {
    label: 651,
    value: 278.942,
  },
  {
    label: 652,
    value: 278.974,
  },
  {
    label: 653,
    value: 279.005,
  },
  {
    label: 654,
    value: 279.036,
  },
  {
    label: 655,
    value: 279.066,
  },
  {
    label: 656,
    value: 279.095,
  },
  {
    label: 657,
    value: 279.124,
  },
  {
    label: 658,
    value: 279.153,
  },
  {
    label: 659,
    value: 279.181,
  },
  {
    label: 660,
    value: 279.208,
  },
  {
    label: 661,
    value: 279.235,
  },
  {
    label: 662,
    value: 279.262,
  },
  {
    label: 663,
    value: 279.288,
  },
  {
    label: 664,
    value: 279.314,
  },
  {
    label: 665,
    value: 279.339,
  },
  {
    label: 666,
    value: 279.364,
  },
  {
    label: 667,
    value: 279.389,
  },
  {
    label: 668,
    value: 279.413,
  },
  {
    label: 669,
    value: 279.436,
  },
  {
    label: 670,
    value: 279.46,
  },
  {
    label: 671,
    value: 279.483,
  },
  {
    label: 672,
    value: 279.505,
  },
  {
    label: 673,
    value: 279.526,
  },
  {
    label: 674,
    value: 279.547,
  },
  {
    label: 675,
    value: 279.567,
  },
  {
    label: 676,
    value: 279.587,
  },
  {
    label: 677,
    value: 279.605,
  },
  {
    label: 678,
    value: 279.623,
  },
  {
    label: 679,
    value: 279.639,
  },
  {
    label: 680,
    value: 279.654,
  },
  {
    label: 681,
    value: 279.668,
  },
  {
    label: 682,
    value: 279.681,
  },
  {
    label: 683,
    value: 279.692,
  },
  {
    label: 684,
    value: 279.702,
  },
  {
    label: 685,
    value: 279.71,
  },
  {
    label: 686,
    value: 279.717,
  },
  {
    label: 687,
    value: 279.722,
  },
  {
    label: 688,
    value: 279.726,
  },
  {
    label: 689,
    value: 279.728,
  },
  {
    label: 690,
    value: 279.728,
  },
  {
    label: 691,
    value: 279.726,
  },
  {
    label: 692,
    value: 279.722,
  },
  {
    label: 693,
    value: 279.716,
  },
  {
    label: 694,
    value: 279.708,
  },
  {
    label: 695,
    value: 279.698,
  },
  {
    label: 696,
    value: 279.686,
  },
  {
    label: 697,
    value: 279.671,
  },
  {
    label: 698,
    value: 279.654,
  },
  {
    label: 699,
    value: 279.634,
  },
  {
    label: 700,
    value: 279.612,
  },
  {
    label: 701,
    value: 279.588,
  },
  {
    label: 702,
    value: 279.562,
  },
  {
    label: 703,
    value: 279.534,
  },
  {
    label: 704,
    value: 279.504,
  },
  {
    label: 705,
    value: 279.472,
  },
  {
    label: 706,
    value: 279.439,
  },
  {
    label: 707,
    value: 279.404,
  },
  {
    label: 708,
    value: 279.369,
  },
  {
    label: 709,
    value: 279.332,
  },
  {
    label: 710,
    value: 279.294,
  },
  {
    label: 711,
    value: 279.256,
  },
  {
    label: 712,
    value: 279.217,
  },
  {
    label: 713,
    value: 279.177,
  },
  {
    label: 714,
    value: 279.137,
  },
  {
    label: 715,
    value: 279.097,
  },
  {
    label: 716,
    value: 279.057,
  },
  {
    label: 717,
    value: 279.017,
  },
  {
    label: 718,
    value: 278.978,
  },
  {
    label: 719,
    value: 278.939,
  },
  {
    label: 720,
    value: 278.9,
  },
  {
    label: 721,
    value: 278.862,
  },
  {
    label: 722,
    value: 278.825,
  },
  {
    label: 723,
    value: 278.789,
  },
  {
    label: 724,
    value: 278.755,
  },
  {
    label: 725,
    value: 278.721,
  },
  {
    label: 726,
    value: 278.689,
  },
  {
    label: 727,
    value: 278.659,
  },
  {
    label: 728,
    value: 278.63,
  },
  {
    label: 729,
    value: 278.604,
  },
  {
    label: 730,
    value: 278.579,
  },
  {
    label: 731,
    value: 278.557,
  },
  {
    label: 732,
    value: 278.536,
  },
  {
    label: 733,
    value: 278.518,
  },
  {
    label: 734,
    value: 278.501,
  },
  {
    label: 735,
    value: 278.486,
  },
  {
    label: 736,
    value: 278.473,
  },
  {
    label: 737,
    value: 278.462,
  },
  {
    label: 738,
    value: 278.452,
  },
  {
    label: 739,
    value: 278.444,
  },
  {
    label: 740,
    value: 278.438,
  },
  {
    label: 741,
    value: 278.432,
  },
  {
    label: 742,
    value: 278.428,
  },
  {
    label: 743,
    value: 278.425,
  },
  {
    label: 744,
    value: 278.423,
  },
  {
    label: 745,
    value: 278.422,
  },
  {
    label: 746,
    value: 278.423,
  },
  {
    label: 747,
    value: 278.424,
  },
  {
    label: 748,
    value: 278.425,
  },
  {
    label: 749,
    value: 278.428,
  },
  {
    label: 750,
    value: 278.431,
  },
  {
    label: 751,
    value: 278.435,
  },
  {
    label: 752,
    value: 278.439,
  },
  {
    label: 753,
    value: 278.444,
  },
  {
    label: 754,
    value: 278.449,
  },
  {
    label: 755,
    value: 278.454,
  },
  {
    label: 756,
    value: 278.459,
  },
  {
    label: 757,
    value: 278.465,
  },
  {
    label: 758,
    value: 278.47,
  },
  {
    label: 759,
    value: 278.475,
  },
  {
    label: 760,
    value: 278.481,
  },
  {
    label: 761,
    value: 278.486,
  },
  {
    label: 762,
    value: 278.49,
  },
  {
    label: 763,
    value: 278.494,
  },
  {
    label: 764,
    value: 278.498,
  },
  {
    label: 765,
    value: 278.501,
  },
  {
    label: 766,
    value: 278.504,
  },
  {
    label: 767,
    value: 278.506,
  },
  {
    label: 768,
    value: 278.508,
  },
  {
    label: 769,
    value: 278.509,
  },
  {
    label: 770,
    value: 278.51,
  },
  {
    label: 771,
    value: 278.51,
  },
  {
    label: 772,
    value: 278.51,
  },
  {
    label: 773,
    value: 278.51,
  },
  {
    label: 774,
    value: 278.51,
  },
  {
    label: 775,
    value: 278.51,
  },
  {
    label: 776,
    value: 278.509,
  },
  {
    label: 777,
    value: 278.508,
  },
  {
    label: 778,
    value: 278.507,
  },
  {
    label: 779,
    value: 278.506,
  },
  {
    label: 780,
    value: 278.506,
  },
  {
    label: 781,
    value: 278.505,
  },
  {
    label: 782,
    value: 278.504,
  },
  {
    label: 783,
    value: 278.504,
  },
  {
    label: 784,
    value: 278.503,
  },
  {
    label: 785,
    value: 278.503,
  },
  {
    label: 786,
    value: 278.504,
  },
  {
    label: 787,
    value: 278.504,
  },
  {
    label: 788,
    value: 278.505,
  },
  {
    label: 789,
    value: 278.507,
  },
  {
    label: 790,
    value: 278.509,
  },
  {
    label: 791,
    value: 278.511,
  },
  {
    label: 792,
    value: 278.514,
  },
  {
    label: 793,
    value: 278.517,
  },
  {
    label: 794,
    value: 278.522,
  },
  {
    label: 795,
    value: 278.526,
  },
  {
    label: 796,
    value: 278.532,
  },
  {
    label: 797,
    value: 278.538,
  },
  {
    label: 798,
    value: 278.545,
  },
  {
    label: 799,
    value: 278.553,
  },
  {
    label: 800,
    value: 278.562,
  },
  {
    label: 801,
    value: 278.572,
  },
  {
    label: 802,
    value: 278.582,
  },
  {
    label: 803,
    value: 278.594,
  },
  {
    label: 804,
    value: 278.606,
  },
  {
    label: 805,
    value: 278.618,
  },
  {
    label: 806,
    value: 278.632,
  },
  {
    label: 807,
    value: 278.646,
  },
  {
    label: 808,
    value: 278.661,
  },
  {
    label: 809,
    value: 278.676,
  },
  {
    label: 810,
    value: 278.692,
  },
  {
    label: 811,
    value: 278.708,
  },
  {
    label: 812,
    value: 278.725,
  },
  {
    label: 813,
    value: 278.742,
  },
  {
    label: 814,
    value: 278.76,
  },
  {
    label: 815,
    value: 278.777,
  },
  {
    label: 816,
    value: 278.796,
  },
  {
    label: 817,
    value: 278.814,
  },
  {
    label: 818,
    value: 278.833,
  },
  {
    label: 819,
    value: 278.852,
  },
  {
    label: 820,
    value: 278.871,
  },
  {
    label: 821,
    value: 278.89,
  },
  {
    label: 822,
    value: 278.909,
  },
  {
    label: 823,
    value: 278.928,
  },
  {
    label: 824,
    value: 278.947,
  },
  {
    label: 825,
    value: 278.967,
  },
  {
    label: 826,
    value: 278.986,
  },
  {
    label: 827,
    value: 279.005,
  },
  {
    label: 828,
    value: 279.024,
  },
  {
    label: 829,
    value: 279.042,
  },
  {
    label: 830,
    value: 279.061,
  },
  {
    label: 831,
    value: 279.079,
  },
  {
    label: 832,
    value: 279.097,
  },
  {
    label: 833,
    value: 279.114,
  },
  {
    label: 834,
    value: 279.131,
  },
  {
    label: 835,
    value: 279.148,
  },
  {
    label: 836,
    value: 279.164,
  },
  {
    label: 837,
    value: 279.18,
  },
  {
    label: 838,
    value: 279.195,
  },
  {
    label: 839,
    value: 279.21,
  },
  {
    label: 840,
    value: 279.224,
  },
  {
    label: 841,
    value: 279.238,
  },
  {
    label: 842,
    value: 279.25,
  },
  {
    label: 843,
    value: 279.262,
  },
  {
    label: 844,
    value: 279.274,
  },
  {
    label: 845,
    value: 279.284,
  },
  {
    label: 846,
    value: 279.294,
  },
  {
    label: 847,
    value: 279.303,
  },
  {
    label: 848,
    value: 279.31,
  },
  {
    label: 849,
    value: 279.317,
  },
  {
    label: 850,
    value: 279.323,
  },
  {
    label: 851,
    value: 279.328,
  },
  {
    label: 852,
    value: 279.332,
  },
  {
    label: 853,
    value: 279.335,
  },
  {
    label: 854,
    value: 279.337,
  },
  {
    label: 855,
    value: 279.337,
  },
  {
    label: 856,
    value: 279.336,
  },
  {
    label: 857,
    value: 279.334,
  },
  {
    label: 858,
    value: 279.331,
  },
  {
    label: 859,
    value: 279.326,
  },
  {
    label: 860,
    value: 279.321,
  },
  {
    label: 861,
    value: 279.314,
  },
  {
    label: 862,
    value: 279.306,
  },
  {
    label: 863,
    value: 279.297,
  },
  {
    label: 864,
    value: 279.288,
  },
  {
    label: 865,
    value: 279.277,
  },
  {
    label: 866,
    value: 279.266,
  },
  {
    label: 867,
    value: 279.254,
  },
  {
    label: 868,
    value: 279.241,
  },
  {
    label: 869,
    value: 279.228,
  },
  {
    label: 870,
    value: 279.214,
  },
  {
    label: 871,
    value: 279.2,
  },
  {
    label: 872,
    value: 279.186,
  },
  {
    label: 873,
    value: 279.171,
  },
  {
    label: 874,
    value: 279.156,
  },
  {
    label: 875,
    value: 279.141,
  },
  {
    label: 876,
    value: 279.126,
  },
  {
    label: 877,
    value: 279.111,
  },
  {
    label: 878,
    value: 279.096,
  },
  {
    label: 879,
    value: 279.082,
  },
  {
    label: 880,
    value: 279.067,
  },
  {
    label: 881,
    value: 279.053,
  },
  {
    label: 882,
    value: 279.039,
  },
  {
    label: 883,
    value: 279.025,
  },
  {
    label: 884,
    value: 279.012,
  },
  {
    label: 885,
    value: 279,
  },
  {
    label: 886,
    value: 278.988,
  },
  {
    label: 887,
    value: 278.977,
  },
  {
    label: 888,
    value: 278.966,
  },
  {
    label: 889,
    value: 278.957,
  },
  {
    label: 890,
    value: 278.948,
  },
  {
    label: 891,
    value: 278.941,
  },
  {
    label: 892,
    value: 278.934,
  },
  {
    label: 893,
    value: 278.929,
  },
  {
    label: 894,
    value: 278.924,
  },
  {
    label: 895,
    value: 278.921,
  },
  {
    label: 896,
    value: 278.92,
  },
  {
    label: 897,
    value: 278.92,
  },
  {
    label: 898,
    value: 278.921,
  },
  {
    label: 899,
    value: 278.923,
  },
  {
    label: 900,
    value: 278.927,
  },
  {
    label: 901,
    value: 278.933,
  },
  {
    label: 902,
    value: 278.939,
  },
  {
    label: 903,
    value: 278.947,
  },
  {
    label: 904,
    value: 278.955,
  },
  {
    label: 905,
    value: 278.965,
  },
  {
    label: 906,
    value: 278.975,
  },
  {
    label: 907,
    value: 278.986,
  },
  {
    label: 908,
    value: 278.998,
  },
  {
    label: 909,
    value: 279.01,
  },
  {
    label: 910,
    value: 279.023,
  },
  {
    label: 911,
    value: 279.036,
  },
  {
    label: 912,
    value: 279.049,
  },
  {
    label: 913,
    value: 279.063,
  },
  {
    label: 914,
    value: 279.077,
  },
  {
    label: 915,
    value: 279.09,
  },
  {
    label: 916,
    value: 279.104,
  },
  {
    label: 917,
    value: 279.117,
  },
  {
    label: 918,
    value: 279.131,
  },
  {
    label: 919,
    value: 279.144,
  },
  {
    label: 920,
    value: 279.156,
  },
  {
    label: 921,
    value: 279.168,
  },
  {
    label: 922,
    value: 279.18,
  },
  {
    label: 923,
    value: 279.19,
  },
  {
    label: 924,
    value: 279.2,
  },
  {
    label: 925,
    value: 279.209,
  },
  {
    label: 926,
    value: 279.217,
  },
  {
    label: 927,
    value: 279.224,
  },
  {
    label: 928,
    value: 279.23,
  },
  {
    label: 929,
    value: 279.235,
  },
  {
    label: 930,
    value: 279.238,
  },
  {
    label: 931,
    value: 279.24,
  },
  {
    label: 932,
    value: 279.241,
  },
  {
    label: 933,
    value: 279.24,
  },
  {
    label: 934,
    value: 279.237,
  },
  {
    label: 935,
    value: 279.233,
  },
  {
    label: 936,
    value: 279.226,
  },
  {
    label: 937,
    value: 279.218,
  },
  {
    label: 938,
    value: 279.208,
  },
  {
    label: 939,
    value: 279.195,
  },
  {
    label: 940,
    value: 279.181,
  },
  {
    label: 941,
    value: 279.164,
  },
  {
    label: 942,
    value: 279.144,
  },
  {
    label: 943,
    value: 279.123,
  },
  {
    label: 944,
    value: 279.098,
  },
  {
    label: 945,
    value: 279.071,
  },
  {
    label: 946,
    value: 279.042,
  },
  {
    label: 947,
    value: 279.011,
  },
  {
    label: 948,
    value: 278.978,
  },
  {
    label: 949,
    value: 278.943,
  },
  {
    label: 950,
    value: 278.908,
  },
  {
    label: 951,
    value: 278.872,
  },
  {
    label: 952,
    value: 278.836,
  },
  {
    label: 953,
    value: 278.799,
  },
  {
    label: 954,
    value: 278.763,
  },
  {
    label: 955,
    value: 278.728,
  },
  {
    label: 956,
    value: 278.694,
  },
  {
    label: 957,
    value: 278.661,
  },
  {
    label: 958,
    value: 278.631,
  },
  {
    label: 959,
    value: 278.602,
  },
  {
    label: 960,
    value: 278.576,
  },
  {
    label: 961,
    value: 278.552,
  },
  {
    label: 962,
    value: 278.532,
  },
  {
    label: 963,
    value: 278.515,
  },
  {
    label: 964,
    value: 278.502,
  },
  {
    label: 965,
    value: 278.493,
  },
  {
    label: 966,
    value: 278.489,
  },
  {
    label: 967,
    value: 278.49,
  },
  {
    label: 968,
    value: 278.496,
  },
  {
    label: 969,
    value: 278.508,
  },
  {
    label: 970,
    value: 278.525,
  },
  {
    label: 971,
    value: 278.547,
  },
  {
    label: 972,
    value: 278.574,
  },
  {
    label: 973,
    value: 278.606,
  },
  {
    label: 974,
    value: 278.642,
  },
  {
    label: 975,
    value: 278.682,
  },
  {
    label: 976,
    value: 278.726,
  },
  {
    label: 977,
    value: 278.774,
  },
  {
    label: 978,
    value: 278.825,
  },
  {
    label: 979,
    value: 278.879,
  },
  {
    label: 980,
    value: 278.935,
  },
  {
    label: 981,
    value: 278.995,
  },
  {
    label: 982,
    value: 279.056,
  },
  {
    label: 983,
    value: 279.119,
  },
  {
    label: 984,
    value: 279.184,
  },
  {
    label: 985,
    value: 279.251,
  },
  {
    label: 986,
    value: 279.318,
  },
  {
    label: 987,
    value: 279.387,
  },
  {
    label: 988,
    value: 279.456,
  },
  {
    label: 989,
    value: 279.525,
  },
  {
    label: 990,
    value: 279.594,
  },
  {
    label: 991,
    value: 279.664,
  },
  {
    label: 992,
    value: 279.732,
  },
  {
    label: 993,
    value: 279.8,
  },
  {
    label: 994,
    value: 279.867,
  },
  {
    label: 995,
    value: 279.933,
  },
  {
    label: 996,
    value: 279.997,
  },
  {
    label: 997,
    value: 280.059,
  },
  {
    label: 998,
    value: 280.119,
  },
  {
    label: 999,
    value: 280.177,
  },
  {
    label: 1000,
    value: 280.232,
  },
  {
    label: 1001,
    value: 280.284,
  },
  {
    label: 1002,
    value: 280.333,
  },
  {
    label: 1003,
    value: 280.379,
  },
  {
    label: 1004,
    value: 280.421,
  },
  {
    label: 1005,
    value: 280.458,
  },
  {
    label: 1006,
    value: 280.492,
  },
  {
    label: 1007,
    value: 280.522,
  },
  {
    label: 1008,
    value: 280.548,
  },
  {
    label: 1009,
    value: 280.572,
  },
  {
    label: 1010,
    value: 280.592,
  },
  {
    label: 1011,
    value: 280.611,
  },
  {
    label: 1012,
    value: 280.627,
  },
  {
    label: 1013,
    value: 280.642,
  },
  {
    label: 1014,
    value: 280.657,
  },
  {
    label: 1015,
    value: 280.67,
  },
  {
    label: 1016,
    value: 280.684,
  },
  {
    label: 1017,
    value: 280.697,
  },
  {
    label: 1018,
    value: 280.712,
  },
  {
    label: 1019,
    value: 280.727,
  },
  {
    label: 1020,
    value: 280.743,
  },
  {
    label: 1021,
    value: 280.762,
  },
  {
    label: 1022,
    value: 280.783,
  },
  {
    label: 1023,
    value: 280.806,
  },
  {
    label: 1024,
    value: 280.833,
  },
  {
    label: 1025,
    value: 280.863,
  },
  {
    label: 1026,
    value: 280.896,
  },
  {
    label: 1027,
    value: 280.934,
  },
  {
    label: 1028,
    value: 280.975,
  },
  {
    label: 1029,
    value: 281.019,
  },
  {
    label: 1030,
    value: 281.067,
  },
  {
    label: 1031,
    value: 281.117,
  },
  {
    label: 1032,
    value: 281.17,
  },
  {
    label: 1033,
    value: 281.226,
  },
  {
    label: 1034,
    value: 281.283,
  },
  {
    label: 1035,
    value: 281.343,
  },
  {
    label: 1036,
    value: 281.404,
  },
  {
    label: 1037,
    value: 281.467,
  },
  {
    label: 1038,
    value: 281.531,
  },
  {
    label: 1039,
    value: 281.596,
  },
  {
    label: 1040,
    value: 281.662,
  },
  {
    label: 1041,
    value: 281.729,
  },
  {
    label: 1042,
    value: 281.796,
  },
  {
    label: 1043,
    value: 281.863,
  },
  {
    label: 1044,
    value: 281.931,
  },
  {
    label: 1045,
    value: 281.998,
  },
  {
    label: 1046,
    value: 282.064,
  },
  {
    label: 1047,
    value: 282.13,
  },
  {
    label: 1048,
    value: 282.194,
  },
  {
    label: 1049,
    value: 282.258,
  },
  {
    label: 1050,
    value: 282.32,
  },
  {
    label: 1051,
    value: 282.38,
  },
  {
    label: 1052,
    value: 282.439,
  },
  {
    label: 1053,
    value: 282.496,
  },
  {
    label: 1054,
    value: 282.55,
  },
  {
    label: 1055,
    value: 282.602,
  },
  {
    label: 1056,
    value: 282.651,
  },
  {
    label: 1057,
    value: 282.697,
  },
  {
    label: 1058,
    value: 282.739,
  },
  {
    label: 1059,
    value: 282.779,
  },
  {
    label: 1060,
    value: 282.815,
  },
  {
    label: 1061,
    value: 282.848,
  },
  {
    label: 1062,
    value: 282.877,
  },
  {
    label: 1063,
    value: 282.904,
  },
  {
    label: 1064,
    value: 282.928,
  },
  {
    label: 1065,
    value: 282.949,
  },
  {
    label: 1066,
    value: 282.968,
  },
  {
    label: 1067,
    value: 282.983,
  },
  {
    label: 1068,
    value: 282.997,
  },
  {
    label: 1069,
    value: 283.008,
  },
  {
    label: 1070,
    value: 283.017,
  },
  {
    label: 1071,
    value: 283.023,
  },
  {
    label: 1072,
    value: 283.028,
  },
  {
    label: 1073,
    value: 283.031,
  },
  {
    label: 1074,
    value: 283.032,
  },
  {
    label: 1075,
    value: 283.031,
  },
  {
    label: 1076,
    value: 283.029,
  },
  {
    label: 1077,
    value: 283.025,
  },
  {
    label: 1078,
    value: 283.02,
  },
  {
    label: 1079,
    value: 283.013,
  },
  {
    label: 1080,
    value: 283.006,
  },
  {
    label: 1081,
    value: 282.997,
  },
  {
    label: 1082,
    value: 282.987,
  },
  {
    label: 1083,
    value: 282.977,
  },
  {
    label: 1084,
    value: 282.966,
  },
  {
    label: 1085,
    value: 282.954,
  },
  {
    label: 1086,
    value: 282.942,
  },
  {
    label: 1087,
    value: 282.929,
  },
  {
    label: 1088,
    value: 282.916,
  },
  {
    label: 1089,
    value: 282.904,
  },
  {
    label: 1090,
    value: 282.89,
  },
  {
    label: 1091,
    value: 282.877,
  },
  {
    label: 1092,
    value: 282.864,
  },
  {
    label: 1093,
    value: 282.852,
  },
  {
    label: 1094,
    value: 282.839,
  },
  {
    label: 1095,
    value: 282.828,
  },
  {
    label: 1096,
    value: 282.816,
  },
  {
    label: 1097,
    value: 282.806,
  },
  {
    label: 1098,
    value: 282.796,
  },
  {
    label: 1099,
    value: 282.787,
  },
  {
    label: 1100,
    value: 282.78,
  },
  {
    label: 1101,
    value: 282.773,
  },
  {
    label: 1102,
    value: 282.768,
  },
  {
    label: 1103,
    value: 282.764,
  },
  {
    label: 1104,
    value: 282.761,
  },
  {
    label: 1105,
    value: 282.76,
  },
  {
    label: 1106,
    value: 282.761,
  },
  {
    label: 1107,
    value: 282.763,
  },
  {
    label: 1108,
    value: 282.767,
  },
  {
    label: 1109,
    value: 282.772,
  },
  {
    label: 1110,
    value: 282.78,
  },
  {
    label: 1111,
    value: 282.788,
  },
  {
    label: 1112,
    value: 282.798,
  },
  {
    label: 1113,
    value: 282.809,
  },
  {
    label: 1114,
    value: 282.822,
  },
  {
    label: 1115,
    value: 282.836,
  },
  {
    label: 1116,
    value: 282.851,
  },
  {
    label: 1117,
    value: 282.867,
  },
  {
    label: 1118,
    value: 282.885,
  },
  {
    label: 1119,
    value: 282.903,
  },
  {
    label: 1120,
    value: 282.922,
  },
  {
    label: 1121,
    value: 282.942,
  },
  {
    label: 1122,
    value: 282.964,
  },
  {
    label: 1123,
    value: 282.986,
  },
  {
    label: 1124,
    value: 283.008,
  },
  {
    label: 1125,
    value: 283.032,
  },
  {
    label: 1126,
    value: 283.056,
  },
  {
    label: 1127,
    value: 283.081,
  },
  {
    label: 1128,
    value: 283.106,
  },
  {
    label: 1129,
    value: 283.132,
  },
  {
    label: 1130,
    value: 283.158,
  },
  {
    label: 1131,
    value: 283.184,
  },
  {
    label: 1132,
    value: 283.211,
  },
  {
    label: 1133,
    value: 283.238,
  },
  {
    label: 1134,
    value: 283.266,
  },
  {
    label: 1135,
    value: 283.293,
  },
  {
    label: 1136,
    value: 283.321,
  },
  {
    label: 1137,
    value: 283.349,
  },
  {
    label: 1138,
    value: 283.377,
  },
  {
    label: 1139,
    value: 283.404,
  },
  {
    label: 1140,
    value: 283.432,
  },
  {
    label: 1141,
    value: 283.46,
  },
  {
    label: 1142,
    value: 283.487,
  },
  {
    label: 1143,
    value: 283.514,
  },
  {
    label: 1144,
    value: 283.541,
  },
  {
    label: 1145,
    value: 283.567,
  },
  {
    label: 1146,
    value: 283.593,
  },
  {
    label: 1147,
    value: 283.618,
  },
  {
    label: 1148,
    value: 283.643,
  },
  {
    label: 1149,
    value: 283.668,
  },
  {
    label: 1150,
    value: 283.691,
  },
  {
    label: 1151,
    value: 283.714,
  },
  {
    label: 1152,
    value: 283.736,
  },
  {
    label: 1153,
    value: 283.758,
  },
  {
    label: 1154,
    value: 283.778,
  },
  {
    label: 1155,
    value: 283.798,
  },
  {
    label: 1156,
    value: 283.817,
  },
  {
    label: 1157,
    value: 283.834,
  },
  {
    label: 1158,
    value: 283.851,
  },
  {
    label: 1159,
    value: 283.866,
  },
  {
    label: 1160,
    value: 283.881,
  },
  {
    label: 1161,
    value: 283.894,
  },
  {
    label: 1162,
    value: 283.906,
  },
  {
    label: 1163,
    value: 283.916,
  },
  {
    label: 1164,
    value: 283.926,
  },
  {
    label: 1165,
    value: 283.935,
  },
  {
    label: 1166,
    value: 283.942,
  },
  {
    label: 1167,
    value: 283.948,
  },
  {
    label: 1168,
    value: 283.954,
  },
  {
    label: 1169,
    value: 283.958,
  },
  {
    label: 1170,
    value: 283.961,
  },
  {
    label: 1171,
    value: 283.964,
  },
  {
    label: 1172,
    value: 283.965,
  },
  {
    label: 1173,
    value: 283.966,
  },
  {
    label: 1174,
    value: 283.965,
  },
  {
    label: 1175,
    value: 283.964,
  },
  {
    label: 1176,
    value: 283.962,
  },
  {
    label: 1177,
    value: 283.959,
  },
  {
    label: 1178,
    value: 283.955,
  },
  {
    label: 1179,
    value: 283.951,
  },
  {
    label: 1180,
    value: 283.946,
  },
  {
    label: 1181,
    value: 283.94,
  },
  {
    label: 1182,
    value: 283.933,
  },
  {
    label: 1183,
    value: 283.926,
  },
  {
    label: 1184,
    value: 283.918,
  },
  {
    label: 1185,
    value: 283.909,
  },
  {
    label: 1186,
    value: 283.9,
  },
  {
    label: 1187,
    value: 283.89,
  },
  {
    label: 1188,
    value: 283.88,
  },
  {
    label: 1189,
    value: 283.869,
  },
  {
    label: 1190,
    value: 283.858,
  },
  {
    label: 1191,
    value: 283.846,
  },
  {
    label: 1192,
    value: 283.833,
  },
  {
    label: 1193,
    value: 283.82,
  },
  {
    label: 1194,
    value: 283.807,
  },
  {
    label: 1195,
    value: 283.794,
  },
  {
    label: 1196,
    value: 283.78,
  },
  {
    label: 1197,
    value: 283.766,
  },
  {
    label: 1198,
    value: 283.751,
  },
  {
    label: 1199,
    value: 283.736,
  },
  {
    label: 1200,
    value: 283.721,
  },
  {
    label: 1201,
    value: 283.706,
  },
  {
    label: 1202,
    value: 283.69,
  },
  {
    label: 1203,
    value: 283.674,
  },
  {
    label: 1204,
    value: 283.658,
  },
  {
    label: 1205,
    value: 283.642,
  },
  {
    label: 1206,
    value: 283.626,
  },
  {
    label: 1207,
    value: 283.61,
  },
  {
    label: 1208,
    value: 283.593,
  },
  {
    label: 1209,
    value: 283.577,
  },
  {
    label: 1210,
    value: 283.56,
  },
  {
    label: 1211,
    value: 283.544,
  },
  {
    label: 1212,
    value: 283.527,
  },
  {
    label: 1213,
    value: 283.51,
  },
  {
    label: 1214,
    value: 283.492,
  },
  {
    label: 1215,
    value: 283.475,
  },
  {
    label: 1216,
    value: 283.457,
  },
  {
    label: 1217,
    value: 283.439,
  },
  {
    label: 1218,
    value: 283.42,
  },
  {
    label: 1219,
    value: 283.401,
  },
  {
    label: 1220,
    value: 283.382,
  },
  {
    label: 1221,
    value: 283.362,
  },
  {
    label: 1222,
    value: 283.342,
  },
  {
    label: 1223,
    value: 283.322,
  },
  {
    label: 1224,
    value: 283.3,
  },
  {
    label: 1225,
    value: 283.279,
  },
  {
    label: 1226,
    value: 283.256,
  },
  {
    label: 1227,
    value: 283.233,
  },
  {
    label: 1228,
    value: 283.21,
  },
  {
    label: 1229,
    value: 283.186,
  },
  {
    label: 1230,
    value: 283.161,
  },
  {
    label: 1231,
    value: 283.135,
  },
  {
    label: 1232,
    value: 283.109,
  },
  {
    label: 1233,
    value: 283.082,
  },
  {
    label: 1234,
    value: 283.054,
  },
  {
    label: 1235,
    value: 283.025,
  },
  {
    label: 1236,
    value: 282.995,
  },
  {
    label: 1237,
    value: 282.964,
  },
  {
    label: 1238,
    value: 282.933,
  },
  {
    label: 1239,
    value: 282.901,
  },
  {
    label: 1240,
    value: 282.867,
  },
  {
    label: 1241,
    value: 282.833,
  },
  {
    label: 1242,
    value: 282.797,
  },
  {
    label: 1243,
    value: 282.761,
  },
  {
    label: 1244,
    value: 282.723,
  },
  {
    label: 1245,
    value: 282.684,
  },
  {
    label: 1246,
    value: 282.644,
  },
  {
    label: 1247,
    value: 282.603,
  },
  {
    label: 1248,
    value: 282.561,
  },
  {
    label: 1249,
    value: 282.517,
  },
  {
    label: 1250,
    value: 282.472,
  },
  {
    label: 1251,
    value: 282.426,
  },
  {
    label: 1252,
    value: 282.378,
  },
  {
    label: 1253,
    value: 282.329,
  },
  {
    label: 1254,
    value: 282.279,
  },
  {
    label: 1255,
    value: 282.227,
  },
  {
    label: 1256,
    value: 282.174,
  },
  {
    label: 1257,
    value: 282.12,
  },
  {
    label: 1258,
    value: 282.063,
  },
  {
    label: 1259,
    value: 282.006,
  },
  {
    label: 1260,
    value: 281.947,
  },
  {
    label: 1261,
    value: 281.888,
  },
  {
    label: 1262,
    value: 281.829,
  },
  {
    label: 1263,
    value: 281.769,
  },
  {
    label: 1264,
    value: 281.71,
  },
  {
    label: 1265,
    value: 281.652,
  },
  {
    label: 1266,
    value: 281.595,
  },
  {
    label: 1267,
    value: 281.539,
  },
  {
    label: 1268,
    value: 281.485,
  },
  {
    label: 1269,
    value: 281.433,
  },
  {
    label: 1270,
    value: 281.384,
  },
  {
    label: 1271,
    value: 281.338,
  },
  {
    label: 1272,
    value: 281.295,
  },
  {
    label: 1273,
    value: 281.256,
  },
  {
    label: 1274,
    value: 281.221,
  },
  {
    label: 1275,
    value: 281.19,
  },
  {
    label: 1276,
    value: 281.164,
  },
  {
    label: 1277,
    value: 281.142,
  },
  {
    label: 1278,
    value: 281.126,
  },
  {
    label: 1279,
    value: 281.113,
  },
  {
    label: 1280,
    value: 281.105,
  },
  {
    label: 1281,
    value: 281.101,
  },
  {
    label: 1282,
    value: 281.101,
  },
  {
    label: 1283,
    value: 281.103,
  },
  {
    label: 1284,
    value: 281.109,
  },
  {
    label: 1285,
    value: 281.118,
  },
  {
    label: 1286,
    value: 281.129,
  },
  {
    label: 1287,
    value: 281.142,
  },
  {
    label: 1288,
    value: 281.158,
  },
  {
    label: 1289,
    value: 281.175,
  },
  {
    label: 1290,
    value: 281.193,
  },
  {
    label: 1291,
    value: 281.213,
  },
  {
    label: 1292,
    value: 281.234,
  },
  {
    label: 1293,
    value: 281.256,
  },
  {
    label: 1294,
    value: 281.278,
  },
  {
    label: 1295,
    value: 281.3,
  },
  {
    label: 1296,
    value: 281.322,
  },
  {
    label: 1297,
    value: 281.343,
  },
  {
    label: 1298,
    value: 281.364,
  },
  {
    label: 1299,
    value: 281.384,
  },
  {
    label: 1300,
    value: 281.403,
  },
  {
    label: 1301,
    value: 281.42,
  },
  {
    label: 1302,
    value: 281.435,
  },
  {
    label: 1303,
    value: 281.449,
  },
  {
    label: 1304,
    value: 281.46,
  },
  {
    label: 1305,
    value: 281.469,
  },
  {
    label: 1306,
    value: 281.475,
  },
  {
    label: 1307,
    value: 281.478,
  },
  {
    label: 1308,
    value: 281.478,
  },
  {
    label: 1309,
    value: 281.475,
  },
  {
    label: 1310,
    value: 281.469,
  },
  {
    label: 1311,
    value: 281.46,
  },
  {
    label: 1312,
    value: 281.448,
  },
  {
    label: 1313,
    value: 281.434,
  },
  {
    label: 1314,
    value: 281.417,
  },
  {
    label: 1315,
    value: 281.398,
  },
  {
    label: 1316,
    value: 281.376,
  },
  {
    label: 1317,
    value: 281.353,
  },
  {
    label: 1318,
    value: 281.327,
  },
  {
    label: 1319,
    value: 281.3,
  },
  {
    label: 1320,
    value: 281.27,
  },
  {
    label: 1321,
    value: 281.239,
  },
  {
    label: 1322,
    value: 281.206,
  },
  {
    label: 1323,
    value: 281.172,
  },
  {
    label: 1324,
    value: 281.136,
  },
  {
    label: 1325,
    value: 281.099,
  },
  {
    label: 1326,
    value: 281.06,
  },
  {
    label: 1327,
    value: 281.021,
  },
  {
    label: 1328,
    value: 280.981,
  },
  {
    label: 1329,
    value: 280.939,
  },
  {
    label: 1330,
    value: 280.897,
  },
  {
    label: 1331,
    value: 280.855,
  },
  {
    label: 1332,
    value: 280.811,
  },
  {
    label: 1333,
    value: 280.767,
  },
  {
    label: 1334,
    value: 280.723,
  },
  {
    label: 1335,
    value: 280.679,
  },
  {
    label: 1336,
    value: 280.635,
  },
  {
    label: 1337,
    value: 280.59,
  },
  {
    label: 1338,
    value: 280.546,
  },
  {
    label: 1339,
    value: 280.502,
  },
  {
    label: 1340,
    value: 280.457,
  },
  {
    label: 1341,
    value: 280.414,
  },
  {
    label: 1342,
    value: 280.371,
  },
  {
    label: 1343,
    value: 280.329,
  },
  {
    label: 1344,
    value: 280.287,
  },
  {
    label: 1345,
    value: 280.246,
  },
  {
    label: 1346,
    value: 280.206,
  },
  {
    label: 1347,
    value: 280.167,
  },
  {
    label: 1348,
    value: 280.13,
  },
  {
    label: 1349,
    value: 280.093,
  },
  {
    label: 1350,
    value: 280.058,
  },
  {
    label: 1351,
    value: 280.024,
  },
  {
    label: 1352,
    value: 279.992,
  },
  {
    label: 1353,
    value: 279.961,
  },
  {
    label: 1354,
    value: 279.932,
  },
  {
    label: 1355,
    value: 279.904,
  },
  {
    label: 1356,
    value: 279.877,
  },
  {
    label: 1357,
    value: 279.851,
  },
  {
    label: 1358,
    value: 279.827,
  },
  {
    label: 1359,
    value: 279.803,
  },
  {
    label: 1360,
    value: 279.781,
  },
  {
    label: 1361,
    value: 279.76,
  },
  {
    label: 1362,
    value: 279.74,
  },
  {
    label: 1363,
    value: 279.722,
  },
  {
    label: 1364,
    value: 279.704,
  },
  {
    label: 1365,
    value: 279.688,
  },
  {
    label: 1366,
    value: 279.672,
  },
  {
    label: 1367,
    value: 279.657,
  },
  {
    label: 1368,
    value: 279.643,
  },
  {
    label: 1369,
    value: 279.631,
  },
  {
    label: 1370,
    value: 279.619,
  },
  {
    label: 1371,
    value: 279.608,
  },
  {
    label: 1372,
    value: 279.597,
  },
  {
    label: 1373,
    value: 279.588,
  },
  {
    label: 1374,
    value: 279.579,
  },
  {
    label: 1375,
    value: 279.571,
  },
  {
    label: 1376,
    value: 279.564,
  },
  {
    label: 1377,
    value: 279.557,
  },
  {
    label: 1378,
    value: 279.551,
  },
  {
    label: 1379,
    value: 279.546,
  },
  {
    label: 1380,
    value: 279.541,
  },
  {
    label: 1381,
    value: 279.537,
  },
  {
    label: 1382,
    value: 279.534,
  },
  {
    label: 1383,
    value: 279.531,
  },
  {
    label: 1384,
    value: 279.528,
  },
  {
    label: 1385,
    value: 279.526,
  },
  {
    label: 1386,
    value: 279.524,
  },
  {
    label: 1387,
    value: 279.522,
  },
  {
    label: 1388,
    value: 279.521,
  },
  {
    label: 1389,
    value: 279.521,
  },
  {
    label: 1390,
    value: 279.52,
  },
  {
    label: 1391,
    value: 279.52,
  },
  {
    label: 1392,
    value: 279.52,
  },
  {
    label: 1393,
    value: 279.52,
  },
  {
    label: 1394,
    value: 279.521,
  },
  {
    label: 1395,
    value: 279.522,
  },
  {
    label: 1396,
    value: 279.522,
  },
  {
    label: 1397,
    value: 279.523,
  },
  {
    label: 1398,
    value: 279.524,
  },
  {
    label: 1399,
    value: 279.525,
  },
  {
    label: 1400,
    value: 279.526,
  },
  {
    label: 1401,
    value: 279.527,
  },
  {
    label: 1402,
    value: 279.528,
  },
  {
    label: 1403,
    value: 279.529,
  },
  {
    label: 1404,
    value: 279.529,
  },
  {
    label: 1405,
    value: 279.53,
  },
  {
    label: 1406,
    value: 279.53,
  },
  {
    label: 1407,
    value: 279.531,
  },
  {
    label: 1408,
    value: 279.531,
  },
  {
    label: 1409,
    value: 279.53,
  },
  {
    label: 1410,
    value: 279.53,
  },
  {
    label: 1411,
    value: 279.529,
  },
  {
    label: 1412,
    value: 279.528,
  },
  {
    label: 1413,
    value: 279.527,
  },
  {
    label: 1414,
    value: 279.529,
  },
  {
    label: 1415,
    value: 279.534,
  },
  {
    label: 1416,
    value: 279.544,
  },
  {
    label: 1417,
    value: 279.561,
  },
  {
    label: 1418,
    value: 279.586,
  },
  {
    label: 1419,
    value: 279.62,
  },
  {
    label: 1420,
    value: 279.665,
  },
  {
    label: 1421,
    value: 279.723,
  },
  {
    label: 1422,
    value: 279.794,
  },
  {
    label: 1423,
    value: 279.88,
  },
  {
    label: 1424,
    value: 279.983,
  },
  {
    label: 1425,
    value: 280.104,
  },
  {
    label: 1426,
    value: 280.245,
  },
  {
    label: 1427,
    value: 280.407,
  },
  {
    label: 1428,
    value: 280.591,
  },
  {
    label: 1429,
    value: 280.8,
  },
  {
    label: 1430,
    value: 281.032,
  },
  {
    label: 1431,
    value: 281.276,
  },
  {
    label: 1432,
    value: 281.516,
  },
  {
    label: 1433,
    value: 281.747,
  },
  {
    label: 1434,
    value: 281.97,
  },
  {
    label: 1435,
    value: 282.185,
  },
  {
    label: 1436,
    value: 282.393,
  },
  {
    label: 1437,
    value: 282.592,
  },
  {
    label: 1438,
    value: 282.784,
  },
  {
    label: 1439,
    value: 282.969,
  },
  {
    label: 1440,
    value: 283.146,
  },
  {
    label: 1441,
    value: 283.315,
  },
  {
    label: 1442,
    value: 283.478,
  },
  {
    label: 1443,
    value: 283.633,
  },
  {
    label: 1444,
    value: 283.782,
  },
  {
    label: 1445,
    value: 283.923,
  },
  {
    label: 1446,
    value: 284.058,
  },
  {
    label: 1447,
    value: 284.186,
  },
  {
    label: 1448,
    value: 284.308,
  },
  {
    label: 1449,
    value: 284.423,
  },
  {
    label: 1450,
    value: 284.532,
  },
  {
    label: 1451,
    value: 284.634,
  },
  {
    label: 1452,
    value: 284.73,
  },
  {
    label: 1453,
    value: 284.821,
  },
  {
    label: 1454,
    value: 284.905,
  },
  {
    label: 1455,
    value: 284.984,
  },
  {
    label: 1456,
    value: 285.056,
  },
  {
    label: 1457,
    value: 285.124,
  },
  {
    label: 1458,
    value: 285.185,
  },
  {
    label: 1459,
    value: 285.241,
  },
  {
    label: 1460,
    value: 285.293,
  },
  {
    label: 1461,
    value: 285.338,
  },
  {
    label: 1462,
    value: 285.379,
  },
  {
    label: 1463,
    value: 285.415,
  },
  {
    label: 1464,
    value: 285.445,
  },
  {
    label: 1465,
    value: 285.471,
  },
  {
    label: 1466,
    value: 285.493,
  },
  {
    label: 1467,
    value: 285.51,
  },
  {
    label: 1468,
    value: 285.522,
  },
  {
    label: 1469,
    value: 285.53,
  },
  {
    label: 1470,
    value: 285.534,
  },
  {
    label: 1471,
    value: 285.534,
  },
  {
    label: 1472,
    value: 285.529,
  },
  {
    label: 1473,
    value: 285.521,
  },
  {
    label: 1474,
    value: 285.509,
  },
  {
    label: 1475,
    value: 285.493,
  },
  {
    label: 1476,
    value: 285.474,
  },
  {
    label: 1477,
    value: 285.451,
  },
  {
    label: 1478,
    value: 285.425,
  },
  {
    label: 1479,
    value: 285.395,
  },
  {
    label: 1480,
    value: 285.362,
  },
  {
    label: 1481,
    value: 285.327,
  },
  {
    label: 1482,
    value: 285.288,
  },
  {
    label: 1483,
    value: 285.246,
  },
  {
    label: 1484,
    value: 285.202,
  },
  {
    label: 1485,
    value: 285.154,
  },
  {
    label: 1486,
    value: 285.105,
  },
  {
    label: 1487,
    value: 285.053,
  },
  {
    label: 1488,
    value: 284.998,
  },
  {
    label: 1489,
    value: 284.942,
  },
  {
    label: 1490,
    value: 284.883,
  },
  {
    label: 1491,
    value: 284.822,
  },
  {
    label: 1492,
    value: 284.76,
  },
  {
    label: 1493,
    value: 284.695,
  },
  {
    label: 1494,
    value: 284.629,
  },
  {
    label: 1495,
    value: 284.561,
  },
  {
    label: 1496,
    value: 284.492,
  },
  {
    label: 1497,
    value: 284.421,
  },
  {
    label: 1498,
    value: 284.35,
  },
  {
    label: 1499,
    value: 284.276,
  },
  {
    label: 1500,
    value: 284.202,
  },
  {
    label: 1501,
    value: 284.127,
  },
  {
    label: 1502,
    value: 284.051,
  },
  {
    label: 1503,
    value: 283.975,
  },
  {
    label: 1504,
    value: 283.897,
  },
  {
    label: 1505,
    value: 283.82,
  },
  {
    label: 1506,
    value: 283.741,
  },
  {
    label: 1507,
    value: 283.663,
  },
  {
    label: 1508,
    value: 283.584,
  },
  {
    label: 1509,
    value: 283.505,
  },
  {
    label: 1510,
    value: 283.426,
  },
  {
    label: 1511,
    value: 283.347,
  },
  {
    label: 1512,
    value: 283.269,
  },
  {
    label: 1513,
    value: 283.19,
  },
  {
    label: 1514,
    value: 283.113,
  },
  {
    label: 1515,
    value: 283.035,
  },
  {
    label: 1516,
    value: 282.958,
  },
  {
    label: 1517,
    value: 282.882,
  },
  {
    label: 1518,
    value: 282.807,
  },
  {
    label: 1519,
    value: 282.733,
  },
  {
    label: 1520,
    value: 282.66,
  },
  {
    label: 1521,
    value: 282.588,
  },
  {
    label: 1522,
    value: 282.517,
  },
  {
    label: 1523,
    value: 282.448,
  },
  {
    label: 1524,
    value: 282.38,
  },
  {
    label: 1525,
    value: 282.314,
  },
  {
    label: 1526,
    value: 282.249,
  },
  {
    label: 1527,
    value: 282.187,
  },
  {
    label: 1528,
    value: 282.126,
  },
  {
    label: 1529,
    value: 282.067,
  },
  {
    label: 1530,
    value: 282.01,
  },
  {
    label: 1531,
    value: 281.956,
  },
  {
    label: 1532,
    value: 281.903,
  },
  {
    label: 1533,
    value: 281.854,
  },
  {
    label: 1534,
    value: 281.806,
  },
  {
    label: 1535,
    value: 281.762,
  },
  {
    label: 1536,
    value: 281.72,
  },
  {
    label: 1537,
    value: 281.681,
  },
  {
    label: 1538,
    value: 281.645,
  },
  {
    label: 1539,
    value: 281.612,
  },
  {
    label: 1540,
    value: 281.582,
  },
  {
    label: 1541,
    value: 281.556,
  },
  {
    label: 1542,
    value: 281.533,
  },
  {
    label: 1543,
    value: 281.513,
  },
  {
    label: 1544,
    value: 281.497,
  },
  {
    label: 1545,
    value: 281.485,
  },
  {
    label: 1546,
    value: 281.476,
  },
  {
    label: 1547,
    value: 281.472,
  },
  {
    label: 1548,
    value: 281.471,
  },
  {
    label: 1549,
    value: 281.475,
  },
  {
    label: 1550,
    value: 281.483,
  },
  {
    label: 1551,
    value: 281.495,
  },
  {
    label: 1552,
    value: 281.511,
  },
  {
    label: 1553,
    value: 281.532,
  },
  {
    label: 1554,
    value: 281.558,
  },
  {
    label: 1555,
    value: 281.589,
  },
  {
    label: 1556,
    value: 281.624,
  },
  {
    label: 1557,
    value: 281.664,
  },
  {
    label: 1558,
    value: 281.71,
  },
  {
    label: 1559,
    value: 281.761,
  },
  {
    label: 1560,
    value: 281.817,
  },
  {
    label: 1561,
    value: 281.878,
  },
  {
    label: 1562,
    value: 281.944,
  },
  {
    label: 1563,
    value: 282.013,
  },
  {
    label: 1564,
    value: 282.084,
  },
  {
    label: 1565,
    value: 282.156,
  },
  {
    label: 1566,
    value: 282.226,
  },
  {
    label: 1567,
    value: 282.295,
  },
  {
    label: 1568,
    value: 282.36,
  },
  {
    label: 1569,
    value: 282.42,
  },
  {
    label: 1570,
    value: 282.474,
  },
  {
    label: 1571,
    value: 282.52,
  },
  {
    label: 1572,
    value: 282.558,
  },
  {
    label: 1573,
    value: 282.584,
  },
  {
    label: 1574,
    value: 282.599,
  },
  {
    label: 1575,
    value: 282.602,
  },
  {
    label: 1576,
    value: 282.589,
  },
  {
    label: 1577,
    value: 282.561,
  },
  {
    label: 1578,
    value: 282.516,
  },
  {
    label: 1579,
    value: 282.452,
  },
  {
    label: 1580,
    value: 282.368,
  },
  {
    label: 1581,
    value: 282.263,
  },
  {
    label: 1582,
    value: 282.135,
  },
  {
    label: 1583,
    value: 281.984,
  },
  {
    label: 1584,
    value: 281.807,
  },
  {
    label: 1585,
    value: 281.603,
  },
  {
    label: 1586,
    value: 281.372,
  },
  {
    label: 1587,
    value: 281.11,
  },
  {
    label: 1588,
    value: 280.818,
  },
  {
    label: 1589,
    value: 280.495,
  },
  {
    label: 1590,
    value: 280.141,
  },
  {
    label: 1591,
    value: 279.761,
  },
  {
    label: 1592,
    value: 279.358,
  },
  {
    label: 1593,
    value: 278.935,
  },
  {
    label: 1594,
    value: 278.497,
  },
  {
    label: 1595,
    value: 278.047,
  },
  {
    label: 1596,
    value: 277.588,
  },
  {
    label: 1597,
    value: 277.125,
  },
  {
    label: 1598,
    value: 276.66,
  },
  {
    label: 1599,
    value: 276.198,
  },
  {
    label: 1600,
    value: 275.741,
  },
  {
    label: 1601,
    value: 275.294,
  },
  {
    label: 1602,
    value: 274.86,
  },
  {
    label: 1603,
    value: 274.443,
  },
  {
    label: 1604,
    value: 274.046,
  },
  {
    label: 1605,
    value: 273.672,
  },
  {
    label: 1606,
    value: 273.326,
  },
  {
    label: 1607,
    value: 273.012,
  },
  {
    label: 1608,
    value: 272.732,
  },
  {
    label: 1609,
    value: 272.49,
  },
  {
    label: 1610,
    value: 272.29,
  },
  {
    label: 1611,
    value: 272.135,
  },
  {
    label: 1612,
    value: 272.025,
  },
  {
    label: 1613,
    value: 271.958,
  },
  {
    label: 1614,
    value: 271.93,
  },
  {
    label: 1615,
    value: 271.939,
  },
  {
    label: 1616,
    value: 271.982,
  },
  {
    label: 1617,
    value: 272.057,
  },
  {
    label: 1618,
    value: 272.16,
  },
  {
    label: 1619,
    value: 272.289,
  },
  {
    label: 1620,
    value: 272.441,
  },
  {
    label: 1621,
    value: 272.614,
  },
  {
    label: 1622,
    value: 272.803,
  },
  {
    label: 1623,
    value: 273.008,
  },
  {
    label: 1624,
    value: 273.224,
  },
  {
    label: 1625,
    value: 273.45,
  },
  {
    label: 1626,
    value: 273.682,
  },
  {
    label: 1627,
    value: 273.918,
  },
  {
    label: 1628,
    value: 274.155,
  },
  {
    label: 1629,
    value: 274.389,
  },
  {
    label: 1630,
    value: 274.62,
  },
  {
    label: 1631,
    value: 274.845,
  },
  {
    label: 1632,
    value: 275.065,
  },
  {
    label: 1633,
    value: 275.279,
  },
  {
    label: 1634,
    value: 275.486,
  },
  {
    label: 1635,
    value: 275.685,
  },
  {
    label: 1636,
    value: 275.877,
  },
  {
    label: 1637,
    value: 276.06,
  },
  {
    label: 1638,
    value: 276.233,
  },
  {
    label: 1639,
    value: 276.397,
  },
  {
    label: 1640,
    value: 276.55,
  },
  {
    label: 1641,
    value: 276.693,
  },
  {
    label: 1642,
    value: 276.825,
  },
  {
    label: 1643,
    value: 276.946,
  },
  {
    label: 1644,
    value: 277.057,
  },
  {
    label: 1645,
    value: 277.158,
  },
  {
    label: 1646,
    value: 277.25,
  },
  {
    label: 1647,
    value: 277.332,
  },
  {
    label: 1648,
    value: 277.406,
  },
  {
    label: 1649,
    value: 277.47,
  },
  {
    label: 1650,
    value: 277.526,
  },
  {
    label: 1651,
    value: 277.574,
  },
  {
    label: 1652,
    value: 277.615,
  },
  {
    label: 1653,
    value: 277.647,
  },
  {
    label: 1654,
    value: 277.673,
  },
  {
    label: 1655,
    value: 277.691,
  },
  {
    label: 1656,
    value: 277.703,
  },
  {
    label: 1657,
    value: 277.709,
  },
  {
    label: 1658,
    value: 277.708,
  },
  {
    label: 1659,
    value: 277.702,
  },
  {
    label: 1660,
    value: 277.69,
  },
  {
    label: 1661,
    value: 277.673,
  },
  {
    label: 1662,
    value: 277.651,
  },
  {
    label: 1663,
    value: 277.624,
  },
  {
    label: 1664,
    value: 277.594,
  },
  {
    label: 1665,
    value: 277.559,
  },
  {
    label: 1666,
    value: 277.52,
  },
  {
    label: 1667,
    value: 277.478,
  },
  {
    label: 1668,
    value: 277.433,
  },
  {
    label: 1669,
    value: 277.385,
  },
  {
    label: 1670,
    value: 277.335,
  },
  {
    label: 1671,
    value: 277.283,
  },
  {
    label: 1672,
    value: 277.228,
  },
  {
    label: 1673,
    value: 277.172,
  },
  {
    label: 1674,
    value: 277.114,
  },
  {
    label: 1675,
    value: 277.056,
  },
  {
    label: 1676,
    value: 276.996,
  },
  {
    label: 1677,
    value: 276.937,
  },
  {
    label: 1678,
    value: 276.877,
  },
  {
    label: 1679,
    value: 276.817,
  },
  {
    label: 1680,
    value: 276.757,
  },
  {
    label: 1681,
    value: 276.699,
  },
  {
    label: 1682,
    value: 276.641,
  },
  {
    label: 1683,
    value: 276.585,
  },
  {
    label: 1684,
    value: 276.53,
  },
  {
    label: 1685,
    value: 276.477,
  },
  {
    label: 1686,
    value: 276.426,
  },
  {
    label: 1687,
    value: 276.378,
  },
  {
    label: 1688,
    value: 276.333,
  },
  {
    label: 1689,
    value: 276.29,
  },
  {
    label: 1690,
    value: 276.251,
  },
  {
    label: 1691,
    value: 276.216,
  },
  {
    label: 1692,
    value: 276.184,
  },
  {
    label: 1693,
    value: 276.156,
  },
  {
    label: 1694,
    value: 276.131,
  },
  {
    label: 1695,
    value: 276.11,
  },
  {
    label: 1696,
    value: 276.093,
  },
  {
    label: 1697,
    value: 276.079,
  },
  {
    label: 1698,
    value: 276.07,
  },
  {
    label: 1699,
    value: 276.064,
  },
  {
    label: 1700,
    value: 276.062,
  },
  {
    label: 1701,
    value: 276.063,
  },
  {
    label: 1702,
    value: 276.069,
  },
  {
    label: 1703,
    value: 276.078,
  },
  {
    label: 1704,
    value: 276.092,
  },
  {
    label: 1705,
    value: 276.109,
  },
  {
    label: 1706,
    value: 276.13,
  },
  {
    label: 1707,
    value: 276.156,
  },
  {
    label: 1708,
    value: 276.185,
  },
  {
    label: 1709,
    value: 276.218,
  },
  {
    label: 1710,
    value: 276.256,
  },
  {
    label: 1711,
    value: 276.298,
  },
  {
    label: 1712,
    value: 276.343,
  },
  {
    label: 1713,
    value: 276.394,
  },
  {
    label: 1714,
    value: 276.448,
  },
  {
    label: 1715,
    value: 276.506,
  },
  {
    label: 1716,
    value: 276.569,
  },
  {
    label: 1717,
    value: 276.636,
  },
  {
    label: 1718,
    value: 276.707,
  },
  {
    label: 1719,
    value: 276.783,
  },
  {
    label: 1720,
    value: 276.863,
  },
  {
    label: 1721,
    value: 276.948,
  },
  {
    label: 1722,
    value: 277.037,
  },
  {
    label: 1723,
    value: 277.13,
  },
  {
    label: 1724,
    value: 277.227,
  },
  {
    label: 1725,
    value: 277.325,
  },
  {
    label: 1726,
    value: 277.422,
  },
  {
    label: 1727,
    value: 277.515,
  },
  {
    label: 1728,
    value: 277.6,
  },
  {
    label: 1729,
    value: 277.676,
  },
  {
    label: 1730,
    value: 277.739,
  },
  {
    label: 1731,
    value: 277.787,
  },
  {
    label: 1732,
    value: 277.817,
  },
  {
    label: 1733,
    value: 277.826,
  },
  {
    label: 1734,
    value: 277.811,
  },
  {
    label: 1735,
    value: 277.771,
  },
  {
    label: 1736,
    value: 277.709,
  },
  {
    label: 1737,
    value: 277.627,
  },
  {
    label: 1738,
    value: 277.531,
  },
  {
    label: 1739,
    value: 277.424,
  },
  {
    label: 1740,
    value: 277.311,
  },
  {
    label: 1741,
    value: 277.195,
  },
  {
    label: 1742,
    value: 277.082,
  },
  {
    label: 1743,
    value: 276.973,
  },
  {
    label: 1744,
    value: 276.874,
  },
  {
    label: 1745,
    value: 276.783,
  },
  {
    label: 1746,
    value: 276.7,
  },
  {
    label: 1747,
    value: 276.625,
  },
  {
    label: 1748,
    value: 276.557,
  },
  {
    label: 1749,
    value: 276.498,
  },
  {
    label: 1750,
    value: 276.445,
  },
  {
    label: 1751,
    value: 276.401,
  },
  {
    label: 1752,
    value: 276.363,
  },
  {
    label: 1753,
    value: 276.332,
  },
  {
    label: 1754,
    value: 276.307,
  },
  {
    label: 1755,
    value: 276.291,
  },
  {
    label: 1756,
    value: 276.281,
  },
  {
    label: 1757,
    value: 276.28,
  },
  {
    label: 1758,
    value: 276.286,
  },
  {
    label: 1759,
    value: 276.301,
  },
  {
    label: 1760,
    value: 276.324,
  },
  {
    label: 1761,
    value: 276.356,
  },
  {
    label: 1762,
    value: 276.397,
  },
  {
    label: 1763,
    value: 276.447,
  },
  {
    label: 1764,
    value: 276.507,
  },
  {
    label: 1765,
    value: 276.575,
  },
  {
    label: 1766,
    value: 276.649,
  },
  {
    label: 1767,
    value: 276.726,
  },
  {
    label: 1768,
    value: 276.806,
  },
  {
    label: 1769,
    value: 276.884,
  },
  {
    label: 1770,
    value: 276.96,
  },
  {
    label: 1771,
    value: 277.03,
  },
  {
    label: 1772,
    value: 277.093,
  },
  {
    label: 1773,
    value: 277.146,
  },
  {
    label: 1774,
    value: 277.187,
  },
  {
    label: 1775,
    value: 277.217,
  },
  {
    label: 1776,
    value: 277.243,
  },
  {
    label: 1777,
    value: 277.272,
  },
  {
    label: 1778,
    value: 277.312,
  },
  {
    label: 1779,
    value: 277.371,
  },
  {
    label: 1780,
    value: 277.456,
  },
  {
    label: 1781,
    value: 277.575,
  },
  {
    label: 1782,
    value: 277.729,
  },
  {
    label: 1783,
    value: 277.915,
  },
  {
    label: 1784,
    value: 278.128,
  },
  {
    label: 1785,
    value: 278.364,
  },
  {
    label: 1786,
    value: 278.618,
  },
  {
    label: 1787,
    value: 278.885,
  },
  {
    label: 1788,
    value: 279.162,
  },
  {
    label: 1789,
    value: 279.443,
  },
  {
    label: 1790,
    value: 279.724,
  },
  {
    label: 1791,
    value: 280,
  },
  {
    label: 1792,
    value: 280.268,
  },
  {
    label: 1793,
    value: 280.521,
  },
  {
    label: 1794,
    value: 280.757,
  },
  {
    label: 1795,
    value: 280.97,
  },
  {
    label: 1796,
    value: 281.163,
  },
  {
    label: 1797,
    value: 281.342,
  },
  {
    label: 1798,
    value: 281.514,
  },
  {
    label: 1799,
    value: 281.685,
  },
  {
    label: 1800,
    value: 281.863,
  },
  {
    label: 1801,
    value: 282.05,
  },
  {
    label: 1802,
    value: 282.242,
  },
  {
    label: 1803,
    value: 282.436,
  },
  {
    label: 1804,
    value: 282.628,
  },
  {
    label: 1805,
    value: 282.814,
  },
  {
    label: 1806,
    value: 282.991,
  },
  {
    label: 1807,
    value: 283.156,
  },
  {
    label: 1808,
    value: 283.303,
  },
  {
    label: 1809,
    value: 283.431,
  },
  {
    label: 1810,
    value: 283.534,
  },
  {
    label: 1811,
    value: 283.61,
  },
  {
    label: 1812,
    value: 283.655,
  },
  {
    label: 1813,
    value: 283.665,
  },
  {
    label: 1814,
    value: 283.637,
  },
  {
    label: 1815,
    value: 283.567,
  },
  {
    label: 1816,
    value: 283.461,
  },
  {
    label: 1817,
    value: 283.326,
  },
  {
    label: 1818,
    value: 283.17,
  },
  {
    label: 1819,
    value: 283.002,
  },
  {
    label: 1820,
    value: 282.828,
  },
  {
    label: 1821,
    value: 282.658,
  },
  {
    label: 1822,
    value: 282.499,
  },
  {
    label: 1823,
    value: 282.359,
  },
  {
    label: 1824,
    value: 282.245,
  },
  {
    label: 1825,
    value: 282.167,
  },
  {
    label: 1826,
    value: 282.131,
  },
  {
    label: 1827,
    value: 282.145,
  },
  {
    label: 1828,
    value: 282.206,
  },
  {
    label: 1829,
    value: 282.309,
  },
  {
    label: 1830,
    value: 282.448,
  },
  {
    label: 1831,
    value: 282.616,
  },
  {
    label: 1832,
    value: 282.809,
  },
  {
    label: 1833,
    value: 283.02,
  },
  {
    label: 1834,
    value: 283.244,
  },
  {
    label: 1835,
    value: 283.474,
  },
  {
    label: 1836,
    value: 283.708,
  },
  {
    label: 1837,
    value: 283.946,
  },
  {
    label: 1838,
    value: 284.187,
  },
  {
    label: 1839,
    value: 284.433,
  },
  {
    label: 1840,
    value: 284.681,
  },
  {
    label: 1841,
    value: 284.93,
  },
  {
    label: 1842,
    value: 285.178,
  },
  {
    label: 1843,
    value: 285.423,
  },
  {
    label: 1844,
    value: 285.664,
  },
  {
    label: 1845,
    value: 285.9,
  },
  {
    label: 1846,
    value: 286.142,
  },
  {
    label: 1847,
    value: 286.397,
  },
  {
    label: 1848,
    value: 286.648,
  },
  {
    label: 1849,
    value: 286.874,
  },
  {
    label: 1850,
    value: 287.058,
  },
  {
    label: 1851,
    value: 287.19,
  },
  {
    label: 1852,
    value: 287.263,
  },
  {
    label: 1853,
    value: 287.272,
  },
  {
    label: 1854,
    value: 287.223,
  },
  {
    label: 1855,
    value: 287.129,
  },
  {
    label: 1856,
    value: 287.002,
  },
  {
    label: 1857,
    value: 286.853,
  },
  {
    label: 1858,
    value: 286.694,
  },
  {
    label: 1859,
    value: 286.534,
  },
  {
    label: 1860,
    value: 286.386,
  },
  {
    label: 1861,
    value: 286.258,
  },
  {
    label: 1862,
    value: 286.162,
  },
  {
    label: 1863,
    value: 286.107,
  },
  {
    label: 1864,
    value: 286.104,
  },
  {
    label: 1865,
    value: 286.161,
  },
  {
    label: 1866,
    value: 286.278,
  },
  {
    label: 1867,
    value: 286.454,
  },
  {
    label: 1868,
    value: 286.685,
  },
  {
    label: 1869,
    value: 286.954,
  },
  {
    label: 1870,
    value: 287.239,
  },
  {
    label: 1871,
    value: 287.53,
  },
  {
    label: 1872,
    value: 287.815,
  },
  {
    label: 1873,
    value: 288.087,
  },
  {
    label: 1874,
    value: 288.332,
  },
  {
    label: 1875,
    value: 288.534,
  },
  {
    label: 1876,
    value: 288.699,
  },
  {
    label: 1877,
    value: 288.837,
  },
  {
    label: 1878,
    value: 288.959,
  },
  {
    label: 1879,
    value: 289.076,
  },
  {
    label: 1880,
    value: 289.199,
  },
  {
    label: 1881,
    value: 289.339,
  },
  {
    label: 1882,
    value: 289.507,
  },
  {
    label: 1883,
    value: 289.715,
  },
  {
    label: 1884,
    value: 289.972,
  },
  {
    label: 1885,
    value: 290.289,
  },
  {
    label: 1886,
    value: 290.664,
  },
  {
    label: 1887,
    value: 291.088,
  },
  {
    label: 1888,
    value: 291.552,
  },
  {
    label: 1889,
    value: 292.046,
  },
  {
    label: 1890,
    value: 292.558,
  },
  {
    label: 1891,
    value: 293.078,
  },
  {
    label: 1892,
    value: 293.591,
  },
  {
    label: 1893,
    value: 294.085,
  },
  {
    label: 1894,
    value: 294.548,
  },
  {
    label: 1895,
    value: 294.965,
  },
  {
    label: 1896,
    value: 295.313,
  },
  {
    label: 1897,
    value: 295.575,
  },
  {
    label: 1898,
    value: 295.767,
  },
  {
    label: 1899,
    value: 295.91,
  },
  {
    label: 1900,
    value: 296.026,
  },
  {
    label: 1901,
    value: 296.139,
  },
  {
    label: 1902,
    value: 296.276,
  },
  {
    label: 1903,
    value: 296.457,
  },
  {
    label: 1904,
    value: 296.697,
  },
  {
    label: 1905,
    value: 296.998,
  },
  {
    label: 1906,
    value: 297.351,
  },
  {
    label: 1907,
    value: 297.738,
  },
  {
    label: 1908,
    value: 298.144,
  },
  {
    label: 1909,
    value: 298.552,
  },
  {
    label: 1910,
    value: 298.952,
  },
  {
    label: 1911,
    value: 299.353,
  },
  {
    label: 1912,
    value: 299.769,
  },
  {
    label: 1913,
    value: 300.206,
  },
  {
    label: 1914,
    value: 300.66,
  },
  {
    label: 1915,
    value: 301.126,
  },
  {
    label: 1916,
    value: 301.595,
  },
  {
    label: 1917,
    value: 302.059,
  },
  {
    label: 1918,
    value: 302.507,
  },
  {
    label: 1919,
    value: 302.93,
  },
  {
    label: 1920,
    value: 303.325,
  },
  {
    label: 1921,
    value: 303.699,
  },
  {
    label: 1922,
    value: 304.06,
  },
  {
    label: 1923,
    value: 304.417,
  },
  {
    label: 1924,
    value: 304.777,
  },
  {
    label: 1925,
    value: 305.135,
  },
  {
    label: 1926,
    value: 305.491,
  },
  {
    label: 1927,
    value: 305.841,
  },
  {
    label: 1928,
    value: 306.185,
  },
  {
    label: 1929,
    value: 306.522,
  },
  {
    label: 1930,
    value: 306.852,
  },
  {
    label: 1931,
    value: 307.185,
  },
  {
    label: 1932,
    value: 307.526,
  },
  {
    label: 1933,
    value: 307.881,
  },
  {
    label: 1934,
    value: 308.257,
  },
  {
    label: 1935,
    value: 308.651,
  },
  {
    label: 1936,
    value: 309.052,
  },
  {
    label: 1937,
    value: 309.448,
  },
  {
    label: 1938,
    value: 309.821,
  },
  {
    label: 1939,
    value: 310.153,
  },
  {
    label: 1940,
    value: 310.422,
  },
  {
    label: 1941,
    value: 310.622,
  },
  {
    label: 1942,
    value: 310.762,
  },
  {
    label: 1943,
    value: 310.855,
  },
  {
    label: 1944,
    value: 310.916,
  },
  {
    label: 1945,
    value: 310.965,
  },
  {
    label: 1946,
    value: 311.022,
  },
  {
    label: 1947,
    value: 311.099,
  },
  {
    label: 1948,
    value: 311.208,
  },
  {
    label: 1949,
    value: 311.356,
  },
  {
    label: 1950,
    value: 311.543,
  },
  {
    label: 1951,
    value: 311.764,
  },
  {
    label: 1952,
    value: 312.025,
  },
  {
    label: 1953,
    value: 312.337,
  },
  {
    label: 1954,
    value: 312.709,
  },
  {
    label: 1955,
    value: 313.143,
  },
  {
    label: 1956,
    value: 313.633,
  },
  {
    label: 1957,
    value: 314.172,
  },
  {
    label: 1958,
    value: 314.757,
  },
  {
    label: 1959,
    value: 315.383,
  },
  {
    label: 1960,
    value: 316.042,
  },
  {
    label: 1961,
    value: 316.725,
  },
  {
    label: 1962,
    value: 317.428,
  },
  {
    label: 1963,
    value: 318.153,
  },
  {
    label: 1964,
    value: 318.903,
  },
  {
    label: 1965,
    value: 319.686,
  },
  {
    label: 1966,
    value: 320.51,
  },
  {
    label: 1967,
    value: 321.378,
  },
  {
    label: 1968,
    value: 322.292,
  },
  {
    label: 1969,
    value: 323.252,
  },
  {
    label: 1970,
    value: 324.257,
  },
  {
    label: 1971,
    value: 325.306,
  },
  {
    label: 1972,
    value: 326.397,
  },
  {
    label: 1973,
    value: 327.532,
  },
  {
    label: 1974,
    value: 328.709,
  },
  {
    label: 1975,
    value: 329.929,
  },
  {
    label: 1976,
    value: 331.195,
  },
  {
    label: 1977,
    value: 332.513,
  },
  {
    label: 1978,
    value: 333.88,
  },
  {
    label: 1979,
    value: 335.291,
  },
  {
    label: 1980,
    value: 336.735,
  },
  {
    label: 1981,
    value: 338.204,
  },
  {
    label: 1982,
    value: 339.691,
  },
  {
    label: 1983,
    value: 341.194,
  },
  {
    label: 1984,
    value: 342.711,
  },
  {
    label: 1985,
    value: 344.236,
  },
  {
    label: 1986,
    value: 345.765,
  },
  {
    label: 1987,
    value: 347.293,
  },
  {
    label: 1988,
    value: 348.811,
  },
  {
    label: 1989,
    value: 350.307,
  },
  {
    label: 1990,
    value: 351.772,
  },
  {
    label: 1991,
    value: 353.212,
  },
  {
    label: 1992,
    value: 354.638,
  },
  {
    label: 1993,
    value: 356.071,
  },
  {
    label: 1994,
    value: 357.534,
  },
  {
    label: 1995,
    value: 359.046,
  },
  {
    label: 1996,
    value: 360.615,
  },
  {
    label: 1997,
    value: 362.24,
  },
  {
    label: 1998,
    value: 363.923,
  },
  {
    label: 1999,
    value: 365.656,
  },
  {
    label: 2000,
    value: 367.43,
  },
  {
    label: 2001,
    value: 369.245,
  },
  {
    label: 2002,
    value: 371.102,
  },
  {
    label: 2003,
    value: 372.998,
  },
  {
    label: 2004,
    value: 374.925,
  },
  {
    label: 2005,
    value: 376.876,
  },
  {
    label: 2006,
    value: 378.847,
  },
  {
    label: 2007,
    value: 380.836,
  },
  {
    label: 2008,
    value: 382.849,
  },
  {
    label: 2009,
    value: 384.892,
  },
  {
    label: 2010,
    value: 386.976,
  },
  {
    label: 2011,
    value: 389.11,
  },
  {
    label: 2012,
    value: 391.302,
  },
  {
    label: 2013,
    value: 393.553,
  },
  {
    label: 2014,
    value: 395.857,
  },
  {
    label: 2015,
    value: 398.205,
  },
  {
    label: 2016,
    value: 400.585,
  },
  {
    label: 2017,
    value: 402.982,
  },
  {
    label: 2018,
    value: 405.385,
  },
]

export const labels = filter(input, 30).map(e => e.label)
export const data = filter(input, 30).map(e => e.value)
