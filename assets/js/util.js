export default function (array, step) {
  const returnArray = []

  for (let i = 0; i < array.length; i += step) {
    returnArray.push(array[i])
  }

  return returnArray
}
