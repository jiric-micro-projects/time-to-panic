export default {
  mounted () {
    window.addEventListener('mousemove', this.changeImgPerspective)
    window.addEventListener('scroll', this.scrollListener)

    setInterval(() => { this.increment += 1 }, 30)
  },
  computed: {
    bgPositions () {
      return {
        sun: `background-position-y: ${0.05 * this.scrollTop}%`,
        'hills-2': `background-position-y: ${0.075 * this.scrollTop}%`,
        'hills-1': `background-position-y: ${0.125 * this.scrollTop}%`,
      }
    },
    imgCssY () {
      return {
        'clouds-1': `background-position-x: ${0.025 * this.increment}%`,
        'clouds-2': `background-position-x: -${0.035 * this.increment}%`,
        'clouds-3': `background-position-x: ${0.075 * this.increment}%`,
      }
    },
  },
  data () {
    return {
      body: null,
      increment: 0,
      sxPos: 0,
      syPos: 0,
      screenX: 0,
      screenY: 0,
      scrollTop: 0,
    }
  },
  methods: {
    scrollListener () {
      this.scrollTop = Math.abs(window.pageYOffset)
    },
    changeImgPerspective (a) {
      this.screenY = a.screenY
      this.screenX = a.screenX
      this.sxPos = a.screenX / window.innerWidth * 100 - 50
      this.syPos = a.screenY / window.innerHeight * 100 - 50
    },
    showEl (id) {
      const element = document.getElementById(id)
      const wrap = document.getElementById(`${id}-wrap`)

      function unfade (el) {
        const ele = el
        var op = 0 // initial opacity
        var timer = setInterval(() => {
          if (op >= 1) {
            clearInterval(timer)
          }
          ele.style.opacity = op
          op = op + 0.1
        }, 10)
      }

      const unwatch = this.$watch('scrollTop', () => {
        if (this.isElementInViewport(element)) {
          this.load()
          unfade(wrap)
          unwatch()
        }
      })
    },
    isElementInViewport (el) {
      const rect = el.getBoundingClientRect()

      return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        rect.right <= (window.innerWidth || document.documentElement.clientWidth)
      );
    }
  },
}
