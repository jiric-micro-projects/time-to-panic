import dayjs from 'dayjs'
import Vue from 'vue'

// NUMBERS
Vue.filter('percent', (v, left, decimal = 2) => {
  const output = (v * 100).toFixed(decimal)
  return (left) ? `%${output}` : `${output}%`
})

Vue.filter('fixed', (v, decimal = 2) => v.toFixed(decimal))
Vue.filter('abs', v => Math.abs(v))
Vue.filter('ceil', input => Math.ceil(input))

// DATES
Vue.filter('relative', v => dayjs(v).fromNow())
Vue.filter('month', v => dayjs(v).format('D[.] MMM'))
Vue.filter('date', v => (v) ? dayjs(v).format('D/M/YYYY') : '')
Vue.filter('simple', v => (v) ? dayjs(v).format('D. M. YYYY') : '')
Vue.filter('time', v => dayjs(v).format('D/M/YYYY - hh:ss'))
Vue.filter('humanize', v => {
  const final = dayjs().add(v, 'days').from(dayjs(), true)
  return (final === 'a few seconds') ? 0 : final
})

// TEXT
Vue.filter('ellipsis', (v, limit = 500, finish = '...') => (v.length < 500) ? v : v.substring(0, limit) + finish)
