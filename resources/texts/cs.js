module.exports = {
  segments: {
    hero: {
      itsTimeToPanic: 'It\'s time to panic',
      notLivingTheBest: 'No, we are not living in the best period of human history',
      houseIsOnFire: 'The house is literally on fire',
      andWeNeedToDoSomethingAboutIt: 'And we need to do something about it!',
    },
    globalWarming: {
      theEarthIs: 'The earth is',
      fuckingBurning: 'F*****G burning!',
      paragraph: 'CO2 emissions of our planet over time:',
    },
    theories: {
      header1: 'Ridiculous conspiracy theories are becomming mainstream.',
      header2: 'As the trust in institutions is eroding, so is the trust in everything else.',
      paragraph: 'Percentage of people believing in certain conspiracy theories:',
    },
    iq: {
      header1: 'We are literally getting retarded',
    },
    stds: {
      header1: 'And why the hell does everyone have STDs?!',
      paragraph: 'Number of people tested positive for specific sexually transmited disorder over time:',
    },
    premartial: {
      header1: 'oh.. that\'s why',
      paragraph: 'People having premartial sex:'
    },
    porn: {
      header1: 'We are more loking at porn than having real relationships.',
      paragraph: 'While porn consumption is on the rise, traditional relationships are less prevalent. Why is that a problem?'
    },
    debt: {
      header1: 'While every student in USA is in debt',
      paragraph: 'Cumulative size of debt in USA over time:',
    },
    school: {
      header1: 'Despite the fact, that colledge is simply not worth it anymore',
      paragraph: 'Business leaders being asked which factor is "Very important" for their managers making hiring decisions:',

    },
    depression: {
      header1: 'Maybe thats why',
      header2: 'Depression is pretty normal at this point.',
      paragraph: 'Number of people in millions globally with a specific condition:',
    },
    drugs: {
      header1: 'And more are dying from drug overdose than ever.',
    },
    suicides: {
      header1: 'And killing themselves.',
      paragraph: 'Suicide in USA per 100 000 people:',
    },
    media: {
      header1: 'Trust in media? All time low.',
      paragraph: '% of people believing the media reports the truth accurately:',
    },
    gov: {
      header1: 'And nobody trusts the government either.',
      header2: 'Everything is collapsing, so why would you trust in those who were supposet to prevent this?',
      paragraph: '% of people who believe the federal government does what is right most of the time:'
    },
    spying: {
      header1: 'As it turns out, they doesn\'t trust us either.',
      header2: 'Mass spying and internet censorship is spreading in every country on the planet.',
    },
  },
}
