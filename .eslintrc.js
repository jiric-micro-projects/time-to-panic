module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parser: 'vue-eslint-parser',
  extends: [
    'standard',
    'plugin:nuxt/recommended',
    'plugin:vue/strongly-recommended',
  ],
  // required to lint *.vue files
  plugins: [
    'vue',
    'nuxt',
  ],
  // add your custom rules here
  rules: {
    // == style
    'semi': 'off',
    'brace-style': 'off',
    'arrow-parens': 'off',
    'comma-dangle': 'off',
    'space-before-function-paren': 'off',
    'eol-last': 'error',
    'no-alert': 'error',
    'space-in-parens': ['error', 'never'],
    'camelcase': ['error', { 'properties': 'always' }],
    'quotes': ['error', 'single', { 'allowTemplateLiterals': true }],

    // == import
    'import/no-cycle': 'error',
    'import/no-commonjs': 'error',
    'import/no-duplicates': 'error',
    'import/no-dynamic-require': 'error',
    'import/namespace': ['error', { allowComputed: true }],

    // == vue
    'vue/max-attributes-per-line': 'off',
    "vue/this-in-template": ['error', 'never'],
    'vue/html-indent': ['error', 2, { alignAttributesVertically: false }],
    'vue/singleline-html-element-content-newline': ['error', {
      ignores: ['a', 'p', 'span', 'label', 'textarea', 'nuxt-link', 'template'],
    }],

    // == logic
    'no-return-await': 'error',
    'prefer-template': 'error',
    'no-nested-ternary': 'error',
    'no-unneeded-ternary': 'error',
    'prefer-arrow-callback': 'error',
    'array-callback-return': 'error',
    'no-shadow': ['error', { 'allow': ['state'] }],
    'no-redeclare': 'error',

    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: [
        'acc', // for reduce accumulators
        'accumulator', // for reduce accumulators
        'state', // for ReactRouter context
      ]
    }],

    'no-restricted-syntax': [
      'error',
      {
        selector: 'ForInStatement',
        message: 'for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array.',
      },
      {
        selector: 'ForOfStatement',
        message: 'iterators/generators require regenerator-runtime, which is too heavyweight for this guide to allow them. Separately, loops should be avoided in favor of array iterations.',
      },
      {
        selector: 'LabeledStatement',
        message: 'Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand.',
      },
      {
        selector: 'WithStatement',
        message: '`with` is disallowed in strict mode because it makes code impossible to predict and optimize.',
      },
    ],

    // == production
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off'
  }
}
